Feature: Rounding Setting
Scenario: Done - edit rounding
Given on Rounding sidebar
When touching the Rounding type drop-down
Then the Rounding type drop-down has a drop-down show No Rounding, Rounding Up, Rounding Down, and Rounding Up and Down
And Select some word
Then show the word you select on the Rounding type drop-down
And touching the Rounding amount drop-down
Then the Rounding amount drop-down has a drop-down to show 1.00, 0.50, 0.25, 0.10, 0.05
And select some number
Then show the number you select on the Rounding amount drop-down
And touching the DONE button
Then toast Loading… has shown up and the Rounding setting sidebar has faded to the right side from the left side


Scenario: Cancel - edit rounding
Given on Rounding sidebar
When touching the Rounding type drop-down
Then the Rounding type drop-down has a drop-down show No Rounding, Rounding Up, Rounding Down, and Rounding Up and Down
And Select some word
Then show the word you select on the Rounding type drop-down
And touching the Rounding amount drop-down
Then the Rounding amount drop-down has a drop-down to show 1.00, 0.50, 0.25, 0.10, 0.05
And select some number
Then show the number you select on the Rounding amount drop-down
And touching the DONE button
Then toast Loading… has shown up and the Rounding setting sidebar has faded to the right side from the left side
