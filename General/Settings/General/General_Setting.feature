Feature: General Setting
Scenario: Done - Edit General
Given on storefront page
When touching W button
Then sidebar has faded out from the left side

When touching Setting
Then sidebar has changed to a setting sidebar
And touching General
Then the Setting sidebar has faded out from the right side to General setting 

When touching the Currency textbox
Then can not edit the Currency textbox

When touching the Bill discount type toggle
Then toggle has moved to Sub Total
And fix it back to normal

When touching the Max Discount textbox
Then can edit a number with a limit of 100%
And fix it back to normal

When touching the Service Rate textbox
Then can edit a number with a limit of 100%
And fix it back to normal

When touching the Except Take Away toggle
Then toggle has moved from the left side to the right side with orange color
And fix it back to normal

When touching the Except Delivery toggle
Then toggle has moved from the left side to the right side with orange color
And fix it back to normal

When touching the TAX Name drop-down box
Then drop-down box has a drop-down to show TAX, VAT, and GST
And select someone
Then drop-down box has show word of your selection on the box
And fix it back to normal

When touching the TAX Rate textbox
Then can edit a number with a limit of 100%
And fix it back to normal

When touching the DONE button
Then toast Updating… and the general setting page changed to storefront page 


Scenario: Cancel - Edit General
Given on storefront page
When touching W button
Then sidebar has faded out from the left side

When touching Setting
Then sidebar has changed to a setting sidebar
And touching General
Then the Setting sidebar has faded out from the right side to General setting 

When touching the Currency textbox
Then can not edit the Currency textbox

When touching the Bill discount type toggle
Then toggle has moved to Sub Total
And fix it back to normal

When touching the Max Discount textbox
Then can edit a number with a limit of 100%
And fix it back to normal

When touching the Service Rate textbox
Then can edit a number with a limit of 100%
And fix it back to normal

When touching the Except Take Away toggle
Then toggle has moved from the left side to the right side with orange color
And fix it back to normal

When touching the Except Delivery toggle
Then toggle has moved from the left side to the right side with orange color
And fix it back to normal

When touching the TAX Name drop-down box
Then drop-down box has a drop-down to show TAX, VAT, and GST
And select someone
Then drop-down box has show word of your selection on the box
And fix it back to normal

When touching the TAX Rate textbox
Then can edit a number with a limit of 100%
And fix it back to normal

When touching the arrow top with left side button
Then the General setting sidebar has faded out from the left side to the Setting sidebar
