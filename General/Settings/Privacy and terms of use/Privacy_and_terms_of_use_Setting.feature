Feature: Privacy and terms of use Setting
Scenario: Terms of use
Given on Setting sidebar
When touching the Setting Privacy and term of use
Then the Privacy and term of use page has to fade up from the bottom side

When touching the Terms of use box
Then the Terms of Condition page has faded from the right side to the left side
And check a word, paragraph on this page


Scenario: Collecting usage data
Given on Setting sidebar
When touching the Collecting usage data box
Then the Collecting usage data page has faded from the right side to the left side
And check a word on this page

When touching the Collecting usage data toggle
Then the Collecting usage data toggle has faded to the left side from the right side with gray color
And fix it back to normal

When touching the Learn more about our privacy policy
Then the Terms of Condition page has faded from the right side to the left side
And check a word, paragraph on this page
