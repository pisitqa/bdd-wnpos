Feature: Drawer Setting
Scenario: Drawer Setting
Given on Drawer Setting page
When touching the Drawer toggle
Then The Drawer toggle has moved from the left side to the right with orange and show Default Amount, Check open bill before ending, Show Detail, Email, and Category

When touching the Default Amount textbox
Then can edit
And fix it back to normal

When touching the Check open bill before ending toggle
Then the Check open bill before ending toggle has moved to the left side form the right side with gray color
And Fix it back to normal

When touching the Show Detail toggle
Then the Show Detail toggle has moved to the right from the left side with orange color
And fix it back to normal

When touching the Auto Email report toggle
Then the Auto Email report toggle has moved to the right from the left side with an orange color and Set Email button can use
And touching the Set Email button
Then Setting Email has a pop-up

When touching the Setting Email Textbox
Then can edit with correct email format
And touching the ADD button
Then email can move to show on the box under the ADD button
And touching the DONE button
Then Setting Email has close
And fix it back to normal

When touching the Auto Email report toggle
Then the Auto Email report toggle has moved to the right from the left side with an orange color and Set Email button can use
And touching the Set Email button
Then Setting Email has a pop-up

When touching the Setting Email Textbox
Then can edit with wrong email format
And Warning Email format is incorrect has shown up
Then touching the OK button
And Warning Email format is incorrect has close
Then touching the DONE button
And Setting Email has close

When touching the Auto Email report toggle
Then the Auto Email report toggle has moved to the right from the left side with an orange color and Set Email button can use
And touching the Set Email button
Then Setting Email has a pop-up

When touching the Setting Email Textbox
Then can edit with correct email format
And touching the ADD button
Then email can move to show on the box under the ADD button
And touching the CANCEL button
Then Setting Email has close
And touching the Set Email button
Then Setting Email has a pop-up
And check to see if there is an email that was previously entered
Then fix it back to normal


Scenario: Done - Create Paid in Category
Given on Drawer Setting
When touching the Category tag toggle
Then the Category tag toggle has moved to the right side from the left side with orange color
And touching the Set Category button
Then Create Category has a pop-up

When touching the Create Category textbox
Then can edit
And touching the Paid in a radio button
Then orange color has shown up on the Paid in a radio button
And touching the ADD button
Then some word you edit has moved to the box under the ADD button
And touching the DONE button
Then Create Category has close
And fix it back to normal


Scenario: Cancel - Create Paid in Category
Given on Drawer Setting
When touching the Category tag toggle
Then the Category tag toggle has moved to the right side from the left side with orange color
And touching the Set Category button
Then Create Category has a pop-up

When touching the Create Category textbox
Then can edit
And touching the Paid in a radio button
Then orange color has shown up on the Paid in a radio button
And touching the ADD button
Then some word you edit has moved to the box under the ADD button
And touching the CANCEL button
Then Create Category has close
And touching the Set Category
Then check to see if there is a word that was previously entered
And fix it back to normal


Scenario: Done - Create Paid out Category
Given on Drawer Setting
When touching the Category tag toggle
Then the Category tag toggle has moved to the right side from the left side with orange color
And touching the Set Category button
Then Create Category has a pop-up

When touching the Create Category textbox
Then can edit
And touching the Paid out a radio button
Then orange color has shown up on the Paid out a radio button
And touching the ADD button
Then some word you edit has moved to the box under the ADD button
And touching the DONE button
Then Create Category has close
And fix it back to normal


Scenario: Cancel - Create Paid out Category
Given on Drawer Setting
When touching the Category tag toggle
Then the Category tag toggle has moved to the right side from the left side with orange color
And touching the Set Category button
Then Create Category has a pop-up

When touching the Create Category textbox
Then can edit
And touching the Paid out a radio button
Then orange color has shown up on the Paid out a radio button
And touching the ADD button
Then some word you edit has moved to the box under the ADD button
And touching the CANCEL button
Then Create Category has close
And touching the Set Category
Then check to see if there is a word that was previously entered
And fix it back to normal


Scenario: Paid by Credit on - Drawer Kick
Given on Drawer Setting
When touching the Paid by Credit toggle
Then the Paid by Credit toggle has moved to the right side from the left side with orange color
And fix it back to normal

When touching the Drawer toggle
Then the Drawer toggle has moved to the left side from the right side with gray color
And touching the DONE button
Then toast Updating… and the general setting page changed to storefront page 
