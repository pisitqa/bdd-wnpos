Feature: Report Setting
Scenario: edit Report Setting
Given on the Setting sidebar
When touching the Report
Then the Setting sidebar has faded to the left side from the right side to the Report Setting sidebar

When touching the Change new day drop-down
Then the Change new day drop-down has a drop-down to show 00-23, and 00-59
And select some number
Then a number you select has shown on the Change new day drop-down
And touching the DONE button
Then toast Updating… and the general setting page changed to storefront page
