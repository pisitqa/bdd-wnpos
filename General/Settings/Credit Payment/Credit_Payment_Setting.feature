Feature: Credit Payment Setting
Scenario: Edit Credit Payment
Given on Credit Payment page
When touching the Enable credit card payment toggle
Then the Enable credit card payment toggle has faded from the right side to the left side with gray color

When touching a checkmark front of the row
Then checkmark has missed and the select row will move to the bottom

When touching long-press on the 3 lines back of the row and slide to top or bottom
Then the row has move follow you slide


Scenario: Edit Custom Payment
Given on Credit Payment
When touching the Custom box
Then the white box has changed to the Custom box and a row of Credit has changed to a row of Custom

When touching the Enable Custom Payment toggle
Then the Enable Custom Payment toggle has faded from the right side to the left side with gray color

When touching a checkmark front of the row
Then checkmark has missed