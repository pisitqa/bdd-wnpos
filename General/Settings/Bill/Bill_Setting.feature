Feature: Bill Setting
Scenario: Done - Bill Setting
Given on Bill setting
When touching the Logo drop-down
Then Logo drop-down has a drop-down to show the Logo box

When touching the Logo box
Then Logo box has pop-up a LIBRARY button

When touching the LIBRARY button
And pop-up has changed to Photo pop-up
Then select photo form an album
And the photo has shown on the Logo box
Then Fix it back to normal

When touching the Branch detail drop-down
Then Branch detail drop-down has a drop-down to show the Address textbox and Phone Number textbox

When editing the Address textbox
Then can edit

When editing the Phone Number textbox
Then can edit
And fix it back to normal

When touching the Header text drop-down
Then Header text drop-down has a drop-down to show the Additional Header textbox

When editing the Additional textbox
Then can edit
And fix it back to normal

When touching the Registration TAX Invoice drop-down

Then the Registration TAX Invoice drop-down has a drop-down to show the POS ID Number textbox and TAX ID textbox

When editing the POS ID Number textbox
Then can edit

When editing the TAX ID textbox
Then can edit
And fix it back to normal
When touching the TAX Invoice detail drop-down
Then the TAX Invoice detail drop-down has a drop-down to show the Invoice name textbox and Invoice last number textbox

When editing the Invoice name textbox
Then can edit

When editing the Invoice last number textbox
Then can edit
And fix it back to normal

When touching the Bill Option drop-down
Then the Bill Option drop-down has a drop-down to show the Show option on Invoice & Receipt toggle, Not print menu at price 0 toggle and Print table name toggle

When touching the Show option on Invoice & Receipt toggle
Then the Show option on Invoice & Receipt toggle has faded from the left side with orange color

When touching the Not print menu at price 0 toggle
Then the Not print menu at price 0 toggle has faded from the left side with orange color

When touching the Print table name toggle
Then the Print table name toggle has faded from the left side with orange color

When touching the DONE button
Then toast Loading… has shown up and the Bill setting page has faded down from the top with setting sidebar has faded out from the left side and the Loading... page has a pop-up on the storefront page
And the Loading... page has close when loading passed


Scenario: Cancel - Bill Setting
Given on Bill setting
When touching the Logo drop-down
Then Logo drop-down has a drop-down to show the Logo box

When touching the Logo box
Then Logo box has pop-up a LIBRARY button

When touching the LIBRARY button
And pop-up has changed to Photo pop-up
Then select photo form an album
And the photo has shown on the Logo box
Then Fix it back to normal

When touching the Branch detail drop-down
Then Branch detail drop-down has a drop-down to show the Address textbox and Phone Number textbox

When editing the Address textbox
Then can edit

When editing the Phone Number textbox
Then can edit
And fix it back to normal

When touching the Header text drop-down
Then Header text drop-down has a drop-down to show the Additional Header textbox

When editing the Additional textbox
Then can edit
And fix it back to normal

When touching the Registration TAX Invoice drop-down
Then the Registration TAX Invoice drop-down has a drop-down to show the POS ID Number textbox and TAX ID textbox

When editing the POS ID Number textbox
Then can edit

When editing the TAX ID textbox
Then can edit
And fix it back to normal

When touching the TAX Invoice detail drop-down
Then the TAX Invoice detail drop-down has a drop-down to show the Invoice name textbox and Invoice last number textbox

When editing the Invoice name textbox
Then can edit

When editing the Invoice last number textbox
Then can edit
And fix it back to normal

When touching the Bill Option drop-down
Then the Bill Option drop-down has a drop-down to show the Show option on Invoice & Receipt toggle, Not print menu at price 0 toggle and Print table name toggle

When touching the Show option on Invoice & Receipt toggle
Then the Show option on Invoice & Receipt toggle has faded from the left side with orange color

When touching the Not print menu at price 0 toggle
Then the Not print menu at price 0 toggle has faded from the left side with orange color

When touching the Print table name toggle
Then the Print table name toggle has faded from the left side with orange color

When touching the House button
Then Bill setting page has faded from the top side to the Setting sidebar page
