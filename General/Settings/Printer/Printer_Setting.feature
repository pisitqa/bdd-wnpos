Feature: Printer Setting
Scenario: Done - Setup Name print for Invoice, Receipt and Order Ticket
Given on Printer setting page
When touching the Setup Name print for Invoice, Receipt and Order Ticket box
Then the Setup Name print for Invoice, Receipt and Order Ticket box has a drop-down

When touching the Invoice & Receipt drop-down box
Then the Invoice & Receipt has drop-down to show Main Name and Name 2
And select Name 2 
Then the Invoice & Receipt has show Name 2 on the drop-down box

When touching the Order Ticket drop-down box
Then the Order Ticket has drop-down to show Main Name, Name 2 and Menu Code
And select Name 2
Then the Order Ticket has show Name 2 on the drop-down box

When touching the DONE button
Then toast Updating... has shown up


Scenario: Cancel - Setup Name print for Invoice, Receipt and Order Ticket
Given on Printer setting page
When touching the Setup Name print for Invoice, Receipt and Order Ticket box
Then the Setup Name print for Invoice, Receipt and Order Ticket box has a drop-down

When touching the Invoice & Receipt drop-down box
Then the Invoice & Receipt has drop-down to show Main Name and Name 2
And select Name 2 
Then the Invoice & Receipt has show Name 2 on the drop-down box

When touching the Order Ticket drop-down box
Then the Order Ticket has drop-down to show Main Name, Name 2 and Menu Code
And select Name 2
Then the Order Ticket has show Name 2 on the drop-down box

When touching the House button
Then toast Updating... has shown up


Scenario: Done - Connect a printer
Given on Printer setting page
When touching a printer box in the same network has shown up with gray color
Then printer box has a drop-down the setting printer
And touching the DONE button
Then toast Updating… shown up and printer box has shown up with white color and the word Connected with green color


Scenario: Cancel - Connect a printer
Given on Printer setting page
When touching a printer box in the same network has shown up with gray color
Then printer box has a drop-down the setting printer
And touching the DONE button
Then toast Updating… shown up and printer box has shown up with white color and the word Connected with green color


Scenario: Done - Setup printer menu
Given on Printer setting page
When touching the SETUP PRINTER MENU button with orange color 
Then Setup Printer by Menu page has faded from the right side
And select a category and touching Select Printer button in the row of the category
Then pop-up has shown up

When selecting your printer
Then checkmark has shown up the backside of the row
And touching the DONE button on the pop-up
Then pop-up has close

When touching the DONE button on the Setup Printer by Menu page 
Then the Setup Printer by Menu page has faded out from the left side


Scenario: Cancel - Setup printer menu
Given on Printer setting page
When touching the SETUP PRINTER MENU button with orange color 
Then Setup Printer by Menu page has faded from the right side
And select a category and touching Select Printer button in the row of the category
Then pop-up has shown up

When selecting your printer
Then checkmark has shown up the backside of the row
And touching the DONE button on the pop-up
Then pop-up has close

When touching the DONE button on the Setup Printer by Menu page 
Then the Setup Printer by Menu page has faded out from the left side


Scenario: Recommend printer
Given on Printer setting page
When touching the RECOMMEND PRINTER button
Then the RECOMMEND PRINTER has faded in from the right side
And check information about printer 3 model is correct
Then touching the arrow on the top with the left side
And the RECOMMEND PRINTER page has faded out from the left side

When touching the house button on the top with the left side
Then the Printer Setting page has faded down from the top with setting sidebar has faded out from the left side and the Loading... page has a pop-up on the storefront page
And the Loading... page has close when loading passed
