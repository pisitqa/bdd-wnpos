Feature: Wongnai POS

   Feature Pos

   Scenario: Change Drawer Setting Drawer Default Amount
   Given  on Drawer setting page
   When   I Click Default Amount fill 2000THB
   And    I go black table page
   Then   I Expect Display popup Open Drawer 2000THB

   Scenario: Change Drawer Check open bill before ending Setting page
   Given  on Drawer Setting page
   When   I Click Check open bill before ending
   Then   I Expect Display Don't End Drawer 

   Scenario: Change Drawer Show Detail Setting page
   Given  on Drawer Setting page
   When   I Click Show Detail
   Then   I Expect Display Show Detail Drawer

   Scenario: Change Drawer Multi Cashier Setting page
   Given  on Drawer Setting page
   When   I Click Multi Cashier
   Then   I Expect Display Show Pay Cashier Name

   Scenario: Change Re-Open Bill Drawer Setting page
   Given  on Drawer Setting page
   When   I Click Re-Open Only Current Drawer
   Then   I Expect Display Re-Open all bill

   Scenario: Change Void All Only Drawer Setting page
   Given  on Drawer setting page
   When   I Click Void All Only
   Then   I Expect Display Don't re-open bill Void all Only all bill

   Scenario: Change Email auto email report Drawer setting page
   Given  on Drawer Setting page
   When   I Click Auto Email report
   Then   I Expect Display Auto Email report sent Email End Drawer

   Scenario: Change Category tag Drawer Setting page
   Given  on Drawer Setting page
   When   I Click Set Category
   And    I Select Create Category fill 'ค่าน้ำแข็ง'
   And    I Select Paid in and Paid Out Add 'ค่าน้ำแข็ง' Done
   Then   I Expect Display Paid in and Paid Out Drop List Drawer

   Scenario: Change Drawer Kick Paid by Credit Drawer Setting
   Given  on Drawer Setting page
   When   I Click Paid by Credit
   Then   I Expect Display pay Credit Drawer Kick

   Scenario: Change Drawer Kick Paid by Custom Drawer Setting
   Given  on Drawer Setting page
   When   I Click Paid by Custom
   Then   I Expect Display pay Custom Drawer Kick