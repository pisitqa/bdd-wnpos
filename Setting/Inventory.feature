Feature: Wongnai POS

   Feature Description Setting Inventory Pos

   Scenario: Change Open Custom Po Address Setting Inventory page
   Given  on Inventory Setting page
   When   I Click Function Custom PO Address
   Then   I Expect Display Open Custom PO Address

   Scenario: Change Close Custom Po Address Setting Inventory page
   Given  on Inventory Setting page
   When   I Click Function Custom PO Address
   Then   I Expect Must not show address in Setting Custom PO Address 

   Scenario: Fill TAX ID Setting Inventory page
   Given  on Inventory Setting page
   When   I click Channel TAX ID
   And    I Fill 'xxxxxxxxxxxxx'
   Then   I Expect Display TAX ID When a PO slip is issued

   Scenario: When TAX ID is not added Setting Inventory page
   Given  on Inventory Setting page
   When   I click Channel TAX ID
   And    I Delete 'xxxxxxxxxxxxx'
   Then   I Expect Display The tax ID number must not be shown on the OP sheet

   Scenario: Change Safety Stock Alert Setting Inventory page
   Given  on Inventory Setting page
   When   I Fill 'arnon.3342@gmail.com' ADD
   And    I Select Send time '17.30'
   Then   I Expect Safety Stock Alert Send Email When it's time

   Scenario: Close Safety Stock Alert Setting Inventory page
   Given   on Inventory Setting page
   When    I Click Close Send Email auto
   Then    I Expect Email will not Send Safety Stock Alert