Feature: Wongnai POS

   Feature POS

   Scenario: Change setting bill logo
   Given  on bill setting page
   When   I Click logo
   And    I Select library and Select Photo
   And    I Click Save
   And    I Go back select table 7
   And    I Select 2 people done
   And    I Select menu "xxxxxx" click Order
   And    I Click send Order
   And    I Select table 7 Select PAY
   And    I Click tap for exact amount and Select CASH
   Then   I Expect display logo Photos Selected

   Scenario: Change Setting Bill Remove Photo
   Given  on Bill Setting page
   When   I Click logo
   And    I Select Remove Photo
   And    I Click Save
   And    I Go back select table 7
   And    I Select 2 people done
   And    I Select menu "xxxxxx" click Order
   And    I Click send Order
   And    I Select table 7 Select PAY
   And    I Click tap for exact amount and Select CASH
   Then   I Expect Display Don't Show the logo

   Scenario: Change Setting Bill Bramch detail
   Given  on Bill Setting page
   When   I Click Branch detail
   And    I fill Address 'ซอยรัชดาภิเษก 20,กรุงเทพมหานคร,สามแสนนอก,กรุงเทพมหานคร,10310,ประเทศไทย'
   And    I Click Save
   And    I Go back select table 7
   And    I Select 2 people done
   And    I Select menu "xxxxxx" click Order
   And    I Click send Order
   And    I Select table 7 Select PAY
   And    I Click tap for exact amount and Select CASH
   Then   I Expect Display all bill Show Address

   Scenario: Change Setting Bill Phone Number
   Given  On Bill Setting page
   When   I Click Phone Number
   And    I fill Phone Number '0934449999'
   And    I Click Save
   And    I Go back select table 7
   And    I Select 2 people done
   And    I Select menu "xxxxxx" click Order
   And    I Click send Order
   And    I Select table 7 Select PAY
   And    I Click tap for exact amount and Select CASH
   Then   I Expect Display all bill Show Phone Number

   Scenario: Change Setting Bill Header Text
   Given  on Bill Setting page
   When   I Click Header Text
   And    I Fill Additional Header 'Thank you'
   And    I Click Save
   And    I Go back select table 7
   And    I Select 2 people done
   And    I Select menu "xxxxxx" click Order
   And    I Click send Order
   And    I Select table 7 Select PAY
   And    I Click tap for exact amount and Select CASH
   Then   I Expect Display all bill Show Header 'Thand you'

   Scenario: Change Setting Bill Additional Footer
   Given  on Bill Setting page
   When   I Click Additional Footer
   And    I fill Additional Footer 'Thank you by best'
   And    I Click Save
   And    I Go back select table 7
   And    I Select 2 people done
   And    I Select menu "xxxxxx" click Order
   And    I Click send Order
   And    I Select table 7 Select PAY
   And    I Click tap for exact amount and Select CASH
   Then   I Expect Display All Bill Show Additional Footer 'Thank you by best'

   Scenario: Change Setting Bill POS ID Number
   Given  on Bill Setting page
   When   I Click Registration TAX Invoice
   And    I Select POS ID Number fill 'xxxxxxxxxxxxx'
   And    I Click Save
   And    I Go back select table 7
   And    I Select 2 people done
   And    I Select menu "xxxxxx" click Order
   And    I Click send Order
   And    I Select table 7 Select PAY
   And    I Click tap for exact amount and Select CASH
   Then   I Expect Display All Bill Show Registration TAX Invoice 'xxxxxxxxxxxxx'

   Scenario: Change Setting Bill TAX ID
   Given  on Bill Setting page
   When   I Click Registration TAX Invoice
   And    I Select TAX ID Fill '1111111111111'
   And    I Click Save
   And    I Go back select table 7
   And    I Select 2 people done
   And    I Select menu "xxxxxx" click Order
   And    I Click send Order
   And    I Select table 7 Select PAY
   And    I Click tap for exact amount and Select CASH
   Then   I Expect Display All Bill Show TAX ID '1111111111111'

   Scenario: Change Setting Bill Invoice name
   Given  on Bill Setting page
   When   I Click TAX Invoice detail
   And    I Select Invoice name Fill 'Quick'
   And    I Click Save
   And    I Go back select table 7
   And    I Select 2 people done
   And    I Select menu "xxxxxx" click Order
   And    I Click send Order
   And    I Select table 7 Select PAY
   And    I Click tap for exact amount and Select CASH
   Then   I Expect Display All Bill Show Invoice Detail 'Quick'

   Scenario: Change Setting Bill Invoice last number
   Given  on Bill Setting page
   When   I Click Invoice Detail
   And    I Select Invoice last number fill '0'
   And    I Click Save
   And    I Go back select table 7
   And    I Select 2 people done
   And    I Select menu "xxxxxx" click Order
   And    I Click send Order
   And    I Select table 7 Select Order Click PAY
   And    I Click tap for exact amount and Select CASH
   Then   I Expect display All Bill Show Invoice last Number Start number '1'

   Scenario: Change Setting Bill Display Item name as "Food And Beverage"
   Given  on Bill Setting page
   When   I Click Display Item neme as "Food And Beverage"
   And    I Click Save
   And    I Go back select table 7
   And    I Select 2 people done
   And    I Select menu "xxx" click Order
   And    I Click send Order
   And    I Select table 7 Select PAY
   And    I Click tap for exact amount and Click CASH
   And    I Select click star on right
   And    I Click bills select closed bill and select please select date
   And    I Click Free space and selecd bill Close table 7
   And    I Click Print Full TAX Invoice
   And    I fill Phone Number "0901110000" Search
   And    I Click TAX ID fill "1102500009999"
   And    I Click Personal name or Company name fill "test"
   And    I Click Address fill "123 Thailand"
   And    I Click Print
   Then   I Expect Display Bill TAX Invoice Show "Food And Beverage"

   Scenario: Change Setting Bill Show Option on Invoice & Receipt are on
   Given  on Bill Setting page
   When   I Click Bill Option are on
   And    I Select Show Option on Invoice & Receipt
   And    I Go back select table 7
   And    I Select 2 people done
   And    I Select menu "xxx" click Order
   And    I Click send Order
   And    I Select table 7 Select PAY
   And    I Click tap for exact amount and Click CASH
   And    I Select click star on right
   And    I Click bills select closed bill and select please select date
   And    I Click Free space and selecd bill Close table 7
   And    I Click Print Full TAX Invoice
   And    I fill Phone Number "0901110000" Search
   And    I Click TAX ID fill "1102500009999"
   And    I Click Personal name or Company name fill "test"
   And    I Click Address fill "123 Thailand"
   And    I Click Print
   Then   I Expect Display Option in all bill

   Scenario: Change Setting Bill Not print menu at price 0 are on
   Given  on Bill Setting page
   When   I Click Not print Menu at price 0 are on
   And    I Click Save
   And    I Go back select table 7
   And    I Select 2 people done
   And    I Select menu "xxxxxx" click Order
   And    I Click send Order
   And    I Select table 7 Select PAY
   And    I Click tap for exact amount and Select CASH
   Then   I Expect Display All Bill Don't Show Order Price 0

   Scenario: Change Setting Bill Print table name are on
   Given  on Bill Setting page
   When   I Click Print table name are on
   And    I Click Save
   And    I Go back select table 7
   And    I Select 2 people done
   And    I Select menu "xxxxxx" click Order
   And    I Click send Order
   And    I Select table 7 Select PAY
   And    I Click tap for exact amount and Select CASH
   Then   I Expect Display All Bill Show table name At the head of the bill