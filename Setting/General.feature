Feature: Wongnai POS

    Feature Description Setting general page

    Scenario: Display Currency 
    Given  Display drop list
    When   I Click General
    And    I Click Currency
    Then   I Expect display drop list 
   

    Scenario:  Charge Currency general page
    Given  I Charge THB are CAD
    When   I click Currency
    And    I Change Currency from THB is CAD  
    Then   I Expect display CAD Currency
    And    I Expect all bill Change valid

    Scenario:  Charge Currency all bill page
    Given  Display after THB is CAD
    When   I click Currency  
    And    I Change Currency from THB is CAD 
    Then   I Expect display CAD Currency

    Scenario:  Charge bill discount type Sub total general page
    Given  Charge Sub Total in bill discount
    When   I Click sub total in bill discount type
    And    I Change bill discount type from Sub Total
    And    I Back The Shop and select table 7 fill 2 people
    And    I Select Order food 'สเต็กหมู 200THB,สเต็กหมูซอสพริกไทยดำ200THB' and send order
    And    I pay bill Order discount 5% display Discount Reason 'เพื่อนเจ้าชองร้าน' done
    Then   I Expect display 'สเต็กหมู 200THB,สเต็กหมูซอสพริกไทยดำ200THB discount all 5%'
    And    I Expect display total price 380THB

    Scenario:  Charge bill discount type Grand total general page
    Given   Charge Grand total in bill discount type
    When   I Click Grand total in bill discount type
    And    I Change bill disocount type from Grand Total
    And    I Back The Shop and select table 7 fill 2 people
    And    I Select Order Food 'สเต็กหมู200THB,สเต็กหมูซอสพริกไทยดำ200THB' and Send Order
    And    I pay bill Order discount Then PIN 1271
    And    I Click total percent Discount 5% display Discount Reason 'เพื่อนเจ้าของร้าน' done
    Then   I Expect display 'สเต็กหมู 200THB,สเต็กหมูซอสพริกไทยดำ200THB discount all 5%'
    And    I Expect display total price 380THB

    Scenario: Charge Service Charge General Setting page
    Given  Charge Service Raie   
    When   I Click Service Raie fill 10%
    Then   I Expect display Service Raie 10% Given General Setting page

    Scenario:  Change Service Charge Inclusive General page
    Given  Change Inclusive in Service Charge
    When   I Click Inclusive Service Charge
    And    I Back The Shop add select table 5 fill 2 people
    And    I Select Order 'สเต็กเนื้อโคตรฟิน300THB' Send Order
    And    I Pay bill Order 'สเต็กเนื้อโคตรฟิน300THB'
    Then   I Expect display SVC inclusive 10% in Menu

    Scenario:  Change Service Charge Exclusive General page
    Given  Change Exclusive in Service Charge
    When   I Click Service Raie fill 10%
    And    I Back The Shop add select table 5 fill 2 people
    And    I select Order 'สเต็กเนื้อโคตรฟิน300THB' Send Order
    And    I Pay bill Order 'สเต็กเนื้อโคตรฟิน300THB'
    Then   I Expect display Service Charge 10% in Menu bill 'สเต็กเนื้อโคตรฟิน300THB'
    And    I Expect display all bill 'สเต็กเนื้อโคตรฟิน330THB'

    Scenario:  Change Service Charge Except Take Away General page
    Given  Change Except Take Away in Service Charge
    When   I Click Service Charge Take Away
    And    I Back The Shop select corner right Take Away
    And    I select Order 'สเต็กเนื้อโคตรฟิน300THB' Send Order 
    And    display Take Away fill Phone Nunber and Name click
    And    I pay bill Order Take Away 'สเต็กเนื้อโคตรฟิน330THB'
    Then   I Expect display Order 'สเต็กเนื้อโคตรฟิน330THB'

    Scenario:  Change Service Charge Except Delivery Genaeral paga
    Given  Change Except Delivery in Service Charge
    When   I Click Service Charge Delivery
    And    I Back The Shop select corner right Delivery
    And    I select Order 'สเต็กเนื้อโคตรฟิน300THB' Send Order 
    And    display Delivery fill Phone Nunber and Name and Address Done
    And    I pay bill Order Delivery 'สเต็กเนื้อโคตรฟิน330THB'
    Then   I Expect display Order 'สเต็กเนื้อโคตรฟิน330THB'

    Scenario: Display TAX Setting General page
    Given  Display TAX Drop Down list
    When   I click TAX Name
    Then   I Expect display Drop Down list
    And    I Expect display TAX,VAT,GST

    Scenario:  Change TAX is VAT general page
    Given  Change TAX is VAT
    When   I click TAX 
    And    I Change TAX from TAX is VAT
    Then   I Expect display VAT TAX
    And    I Expect all bill Change valid

    Scenario: Display TAX Rate 7% general page
    Given  I Fill 7% in TAX Rate
    When   I Click TAX Rate
    And    I Fill 7% 
    Then   I Expect display TAX Rate 7%

    Scenario:  Change Inclusive TAX General page
    Given  Change Inclusive TAX And TAX Want Sum Order Menu
    Then   I Click TAX Inclusive
    And    I Click Table 5 fill 2 people
    And    I Select Order 'สเต็กหมู200THB' Send Order
    And    I Pay bill Order 'สเต็กหมู200THB' 
    And    I Expect display TAX Inclusive 7% in Menu
    And    I Expect display all bill VAT 7%

    Scenario:  Change Exclusive TAX General page
    Given  Change Exclusive in TAX And TAX Want Add Order Menu
    When   I Click Exclusive TAX
    And    I Back given The Shop
    And    I Select table 7 fill 2 people select done
    And    I Select Order 'สเต็กหมู200THB' Send Order
    And    I Pay bill Order 'สเต็กหมู200THB' 
    Then    I Expect TAX Exclusive 7% in Menu
    And    I Expect display TAX Exclusive 7% all bill 'สเต็กหมู214THB'

    Scenario:  Change Back to Home general page
    Given  Send Order Menu Don't Back to Home
    When   I Click Back to Home
    And    I back the Shop
    And    I select table 7 fill 2 people select done
    And    I Select Order 'สเต็กเนื้อโคตรฟิน300THB' Send Order
    Then   I Expect display After Send Order Then need stale Order Menu

    Scenario:  Change Back to Home after Order Sending General page
    Given  After Send Order Back to Home
    When   I Click Back to Home
    And    I Select Table 9 fill 2 people select done
    And    I Select Order 'สเต็กเนื้อโคตรฟิน300THB' Send Order
    Then   I Expect display After send Order need back Order Menu