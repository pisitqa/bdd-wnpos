Feature: Wongnai POS

   Feature POS

   Scenario: Change Credit Payment Setting Credit Payment page
   When   on Credit Payment Setting Page
   Then   I Click Enable Credit Card Payment
   And    I Select VISA,Master Card and Done
   And    I Go back select table 7
   And    I Select 2 people done
   And    I Select menu "xxxxxx" click Order
   And    I Click send Order
   And    I Select table 7 Select PAY
   And    I Select Credit Click VISA
   And    I Expect Display VISA and Master Card Given Pay Credit

   Scenario: Change Credit Payment Setting QR Payment page
   Given  on Credit Payment Setting Page
   When   I Click Enable QR Payment and Done
   And    I Go back select table 7
   And    I Select 2 people done
   And    I Select menu "xxxxxx" click Order
   And    I Click send Order
   And    I Select table 7 Select PAY
   And    I Select Credit Click QR Payment
   Then   I Expect Display QR Payment Given Pay Credit

   Scenario: Change Credit Payment Setting Custom Payment Page
   Given  on Credit Payment Page
   When   I Click Enable Custom Payment
   And    I Click New Custom
   And    I Select Custom Credit name Fill 'โอนเงิน'
   And    I Click SAVE
   And    I Go back select table 7
   And    I Select 2 people done
   And    I Select menu "xxxxxx" click Order
   And    I Click send Order
   And    I Select table 7 Select PAY
   And    I Select Credit Click Custom Payment Name 'โอนเงิน'
   Then   I Expect Display Custom payment 'โิอนเงิน' Given pay Credit