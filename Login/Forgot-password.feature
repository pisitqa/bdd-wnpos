Scenario: Correct email entered
Given on login page
When touching the forgot Password?
Then pop-up Reset Password

When entering the correct email
Then pop-up has change page to Success and sends an email with the new password
And touching the Close button
Then pop-up Success has close


Scenario: The wrong email entered
Given on login page
When touching the forgot Password?
Then pop-up Reset Password

When entering the wrong email
Then pop-up Error No account found with this email address
And touching the OK button
Then pop-up Error has close


Scenario: The wrong email entered
Given on login page
When touching the forgot Password?
Then pop-up Reset Password

When entering the wrong format email
Then pop-up Warning Invalid email format
And touching the OK button
Then pop-up Warning has close
