Scenario: Login success
Given on login page
When entering the email and password
Then change to PIN page


Scenario: Login failed because email is wrong
Given on login page
When entering the wrong email and correct password
Then notify pop-up Warning Invalid user or password
And touching the OK button
Then pop-up Warning has close


Scenario: Login failed because the password is wrong
Given on login page
When entering the correct email and wrong password
Then notify pop-up Warning Invalid user or password
And touching the OK button
Then pop-up Warning has close
Can not
