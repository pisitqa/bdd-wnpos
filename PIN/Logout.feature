Scenario: Logout without Start Drawer
Given on PIN page
When touching the Logout button
Then pop-up Confirm Logout
And Select OK
Then PIN page has changed to Login page


Scenario: Logout without Start Drawer
Given on PIN page
When touching the Logout button
Then pop-up Confirm Logout
And Select CANCEL
Then pop-up Confirm Logout has close


Scenario: Logout with Start Drawer
Given on PIN page
When touching the Logout button
Then pop-up Warning Please end drawer before logout
And Select OK
Then pop-up Warning has close