Scenario: Correct PIN
Given on PIN page
When entering the correct PIN
Then toast Loading… and change to storefront page


Scenario: Wrong PIN
Given on PIN page
When entering a wrong PIN 
Then PIN field has changed to PIN Incorrect

