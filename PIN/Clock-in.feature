Scenario: Correct PIN to clock in
Given on PIN page
When touching Clock in/out button
Then PIN page has changed to Clock in/out page
And entering the correct PIN
Then Clock in/out on the PIN field has changed to Clock in


Scenario: Wrong PIN to clock in
Given on PIN page
When touching Clock in/out button
Then PIN page has changed to Clock in/out page
And entering the wrong PIN
Then Clock in/out on the PIN field has changed to PIN Incorrect!


Scenario: Correct PIN to clock out
Given on PIN page
When touching Clock in/out button
Then PIN page has changed to Clock in/out page
And entering the correct PIN
Then Clock in/out on the PIN field has changed to Clock in


Scenario: Wrong PIN to clock out
Given on PIN page
When touching Clock in/out button
Then PIN page has changed to Clock in/out page
And entering the wrong PIN
Then Clock in/out on the PIN field has changed to PIN Incorrect!