Scenario: Correct email entered
Given on PIN page
When touching the Forgot PIN Code?
Then pop-up Forgot PIN Code

When entering the correct email
Then pop-up has change page to Success and sends an email with the new PIN
And touching the close button
Then pop-up has close


Scenario: The wrong email entered
Given on PIN page
When touching the Forgot PIN Code?
Then pop-up Forgot PIN Code

When entering a wrong email
Then pop-up Warning Email does not match registered email
And touching the OK button
Then pop-up Warning has close
