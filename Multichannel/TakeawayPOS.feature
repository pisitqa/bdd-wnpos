Feature: POS Take away

    Feature POS

    Scenario: Open Bill Channel Take away
    Given   on Table page
    When    I Press the button Take away & Deilvery
    And     I Create Bill Take away
    And     I Select Channel Take away
    Then    I Expect Open Bill Take away

    Scenario: Open Bill Channel Take away Custom
    Given   on Table page
    When    I Press the button Take away & Deilvery
    And     I Create Bill Take away
    And     I Select Channel Take away Custom
    Then    I Expect Open Bill Take away Custom

    Scenario: Close Bill Channel Take away Custom
    Given   on Table page
    When    I Open Bill Take away Custom
    And     I Select Menu "xxxxxx" Send order
    And     I Select Number Fill "0967086858" search
    And     I Select Name Fill "BEST"
    And     I click button PAY
    And     I Fill "1000" And Click button CASH
    Then    I Expect Close Bill Take away Custom in POS Success

    Scenario: Change Number bill Take away page Payment Success
    Given   on Payment page
    When    I Open Bill Take away Custom
    And     I Select Menu "xxxxxx" Send order
    And     I Search Number "09" And Done
    And     I Click Button Edit on right
    And     I Select Fill Number "0967086858" search
    Then    I Expect Name Show information Number "0967086858" Success and Correct

    Scenario: Bill Take away payment CASH 1000 B
    Given   on Payment page
    When    I Open Bill Take away Custom
    And     I Select Menu "xxxxxx" Send order
    And     I Search Number "0967086858" And Done
    And     I Select PAY and payment "CASH 1000 B"
    Then    I Expect display Bill payment CASH Success

    Scenario: Bill Take away payment Cedit Card
    Given   on Payment page
    When    I Open Bill Take away Custom
    And     I Select Menu "xxxxxx" Send order
    And     I Search Number "0967086858" And Done
    And     I Select PAY and payment "Cedit Card"
    And     I Click button VISA
    Then    I Expect display Bill payment Cedit Card Success

    Scenario: Bill Take away payment Custom 
    Given   on Payment page
    When    I Open Bill Take away Custom
    And     I Select Menu "xxxxxx" Send order
    And     I Search Number "0967086858" And Done
    And     I Select PAY and payment Cedit
    And     I Click Button Custom payment "โอนเงิน"
    Then    I Expect display Bill payment Name "โอนเงิน" Success