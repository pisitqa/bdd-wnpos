Feature: Openbilldeilvery

    Feature POS

    Scenario: Open Bill Channel Deilvery
    Given   on Table page
    When    I Press the button Take away & Deilvery
    And     I Create Bill Delivery
    And     I Select Channel Delivery
    Then    I Expect Open Bill Delivery 

    Scenario: Open Bill Channel Deilvery Custom
    Given   on Table page
    When    I Press the button Take away & Deilvery
    And     I Create Bill Delivery
    And     I Select Channel Delivery Custom
    Then    I Expect Open Bill Delivery Custom

    Scenario: Close Bill Channel deilvery Custom
    Given   on Table page
    When    I Open Bill Deilvery Custom
    And     I Select Menu "xxxxxx" Send order
    And     I Select Number Fill "0967086858" search
    And     I Select Name Fill "BEST"
    And     I Select Address Fill "Tester" And Done
    And     I click button PAY
    And     I Fill "1000" And Click button CASH
    Then    I Expect Close Bill Deilvery Custom in POS Success

    Scenario: Change Number bill Deilvery page Payment Success
    Given   on Payment page
    When    I Open Bill Deilvery Custom
    And     I Select Menu "xxxxxx" Send order
    And     I Search Number "09" And Done
    And     I Click Button Edit on right
    And     I Select Fill Number "0967086858" search
    Then    I Expect Name and Address Show information Number "0967086858" Success and Correct

    Scenario: Bill Deilvery payment CASH 1000 B
    Given   on Payment page
    When    I Open Bill Deilvery Custom
    And     I Select Menu "xxxxxx" Send order
    And     I Search Number "0967086858" And Done
    And     I Select PAY and payment "CASH" and fill 1000B
    Then    I Expect display Bill payment CASH Success

    Scenario: Bill Deilvery payment Cedit Card
    Given   on Payment page
    When    I Open Bill Deilvery Custom
    And     I Select Menu "xxxxxx" Send order
    And     I Search Number "0967086858" And Done
    And     I Select PAY and payment "Cedit Card"
    And     I Click button VISA
    Then    I Expect display Bill payment Cedit Card Success

    Scenario: Bill Deilvery payment Custom 
    Given   on Payment page
    When    I Open Bill Deilvery Custom
    And     I Select Menu "xxxxxx" Send order
    And     I Search Number "0967086858" And Done
    And     I Select PAY and payment Cedit
    And     I Click Button Custom payment "โอนเงิน"
    Then    I Expect display Bill payment Name "โอนเงิน" Success

        