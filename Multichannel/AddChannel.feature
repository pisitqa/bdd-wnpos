Feature: Multi-Channel Web

    Feature Add Channel

    Scenario: Add Sales channel multi-channel
    Given   on Web Manage data page
    And     I Click Manage data
    And     I Select Sales channel
    And     I Add Sales channel
    And     I Select Sales Channel "Grab"
    And     I Select Charge GP "0%" and Record
    And     I Select All Menu and Record
    And     I Fill "0" add price all menu Channel "Grab" Record
    Then    I Expect display Sales Channel "Grab"

    Scenario: Add Sales Channel Food panda Charge GP "25%"
    Given   on Web Manage data page
    And     I Select Sales channel
    And     I Add Sales channel
    And     I Select Sales Channel "Food panda"
    And     I Select Charge GP "25%" and Record
    And     I Select All Menu and Record
    And     I Fill "0" add price all menu Channel "Food panda" Record
    Then    I Expect display Sales Channel "Food panda" Charge GP "25%"

    Scenario: Change GP charge Sales Channel Grab Charge GP "25%"
    Given   on Web Manage data page
    And     I Select Sales channel
    And     I Select Sales Channel "Grab"
    And     I Click Setting Sales Channel Grab
    And     I Select Charge GP "25%" Record
    Then    I Expect display Sales Channel "Grab" Charge GP "25%" all Menu

    Scenario: Add Price 20B All Menu in Grab
    Given   on Web manage data page
    And     I Select Sales Channel
    And     I Select Sales Channel "Grab"
    And     I Fill Add Price 20B all menu in Grab
    Then    I Expect display menu add price 20B in Grab

    Scenario: Add Price 20% All Menu in Grab
    Given   on Web manage data page
    And     I Select Sales Channel
    And     I Select Sales Channel "Grab"
    And     I Fill Add Price 20% all menu in Grab
    Then    I Expect display menu add price 20% in Grab

    Scenario: Reset Add Price 20% are 0% All Menu in Grab
    Given   on Web manage data page
    And     I Select Sales Channel
    And     I Select Sales Channel "Grab"
    And     I Select Icon Calculator on Add Price all menu in Grab
    And     I Click Reset Add Price and popup Confirm cleanup
    And     I Click Clear data
    Then    I Expect display reset add price 20% are 0% in Grab

    Scenario: Cancel Reset Add Price 20% are 0% All Menu in Grab
    Given   on Web manage data page
    And     I Select Sales Channel
    And     I Select Sales Channel "Grab"
    And     I Select Icon Calculator on Add Price all menu in Grab
    And     I Click Reset Add Price and popup Confirm cleanup
    And     I Click Cancel
    Then    I Expect display add price 20% in Grab

    Scenario: Add Menu Sales Channel Grab 10 Menu
    Given   on Web manage data page
    And     I Select Sales Channel
    And     I Select Sales Channel "Grab"
    And     I Click Add menu
    And     I Select Menu in Category "xxxxxx"
    And     I Select 10 menu and record
    Then    I Expect have menu add 10 menu in Grab

    Scenario: Delete Menu Sales Channel Grab 10 Menu
    Given   on Web manage data page
    And     I Select Sales Channel
    And     I Select Sales Channel "Grab"
    And     I Click Add menu
    And     I Select Menu in Category "xxxxxx"
    And     I Click 10 menu Does not enter the list
    Then    I Expect 10 The selected menu will not be displayed in the Grab list