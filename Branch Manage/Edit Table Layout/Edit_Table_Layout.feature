Feature: Edit Table Layout
Scenario: Done - Edit Table Layout
Given on storefront sidebar
When touching the Edit Table Layout on storefront sidebar 
Then the storefront page is an edit


Scenario: edit Zone
Given on Edit Table Layout
When touching the  Simple Zone 1 box
Then all zone has faded down with Simple Zone 1 and Add Zone box

When touching the Simple Zone 1
Then all zone has faded up and you can edit the name of the Simple Zone 1
And touching the Simple Zone 1 again
Then you can edit
And delete old name and entering some word
Then can entering some word
And fix it back to normal

When touching the Simple Zone 1
Then all zone has faded down
And touching the Add Zone box
Then create a new zone with name is Zone 2 and a white background

When touching the Zone 2
Then all zone has faded up and Simple Zone 1 has changed to Zone 2


Scenario: Done - delete zone
Given on Edit Table Layout with edit Zone
When touching the Zone 2 box
Then all zone has faded down
And touching the x on the upside with the right side
Then Are you sure to delete this zone? has pop-up
And select the OK button
Then Zone 2 has missing


Scenario: add a table
Given on Edit Table Layout
When touching the + button on the bottom side with the right side
Then the table button and the geometry button has moved from the right to the bottom side with the curve move

When touching the table button
Then the box of tables has moved from the right side to the left side
And select some table
Then the table you select has a show on the Table Layout


Scenario: Done - edit table
Given on Edit Table Layout
When touching the table you select
Then pop-up an X button, copy button, and i button
And touching the i button
Then the detail table has a pop-up
And touching the box on the top
Then can edit
And entering some number or some word
Then the box has change number to some number or some word

When touching the size button Small, Medium, and Large
Then the size button has changed to orange color when you touching

When touching the min seat drop-down
Then the Min seat drop-down has a drop-down to show 1-20
And select some number
Then show on the Min seat drop-down

When touching the max seat drop-down
Then the Max seat drop-down has a drop-down to show 1-20
And select some number
Then show on the Max seat drop-down
And touching the DONE button
Then the detail table has missed with save


Scenario: Cancel - edit table
Given on Edit Table Layout
When touching the table you select
Then pop-up an X button, copy button, and i button
And touching the i button
Then the detail table has a pop-up
And touching the box on the top
Then can edit
And entering some number or some word
Then the box has change number to some number or some word

When touching the size button Small, Medium, and Large
Then the size button has changed to orange color when you touching

When touching the min seat drop-down
Then the Min seat drop-down has a drop-down to show 1-20
And select some number
Then show on the Min seat drop-down

When touching the max seat drop-down
Then the Max seat drop-down has a drop-down to show 1-20
And select some number
Then show on the Max seat drop-down
And touching the CANCEL button
Then the detail table has missed with not save


Scenario: copy button on the table
When touching a table
Then pop-up an X button, copy button, and i button
And touching the copy button
Then a table has added 1 more


Scenario: X button success on the table
When touching a table
Then pop-up an X button, copy button, and i button
And touching the X button
Then Are you sure to delete this table? Has a pop-up with OK button & CANCEL button
And select the OK button
Then the table has missing


Scenario: Cancel - the X button on the table
When touching a table
Then pop-up an X button, copy button, and i button
And touching the X button
Then Are you sure to delete this table? Has a pop-up with OK button & CANCEL button
And select the CANCEL button
Then the pop-up has missing


Scenario: edit geometry 
Given on Edit Table Layout
When touching the geometry button
Then the box of tables has changed to the box of geometry
And select some furniture
Then the furniture you select has a show on the Table Layout

When touching the geometry
Then shown up an X Button, copy button, expand button, and rotate button


Scenario: copy button on the table
When touching a table
Then pop-up an X button, copy button, and i button
And touching the copy button
Then furniture has added 1 more


Scenario: expand button on the table
When touching a table
Then pop-up an X button, copy button, and i button
And long touching the expand button and move left, right, up, down
Then furniture has expanded or shrink


Scenario: rotate button on the table
When touching a table
Then pop-up an X button, copy button, and i button
And long touching the rotate button and move left, right, up, down
Then furniture has to rotate


Scenario: X button success on the furniture
When touching a furniture
Then pop-up an X button, copy button, and i button
And touching the X button
Then Are you sure to delete this object? Has a pop-up with OK button & CANCEL button
And select the OK button
Then the furniture has missing


Scenario: cancel the X button on the furniture
When touching a furniture
Then pop-up an X button, copy button, and i button
And touching the X button
Then Are you sure to delete this object? Has a pop-up with OK button & CANCEL button
And select the CANCEL button
Then the pop-up has missing

When touching the DONE button on the top of the right side
Then toast Saving… has a pop-up and missing when saving done


Feature: Cancel Edit Table Layout
Scenario: Edit Table Layout success
Given on storefront sidebar
When touching the Edit Table Layout on storefront sidebar 
Then the storefront page is an edit


Scenario: edit Zone
Given on Edit Table Layout
When touching the  Simple Zone 1 box
Then all zone has faded down with Simple Zone 1 and Add Zone box

When touching the Simple Zone 1
Then all zone has faded up and you can edit the name of the Simple Zone 1
And touching the Simple Zone 1 again
Then you can edit
And delete old name and entering some word
Then can entering some word
And fix it back to normal

When touching the Simple Zone 1
Then all zone has faded down
And touching the Add Zone box
Then create a new zone with name is Zone 2 and a white background

When touching the Zone 2
Then all zone has faded up and Simple Zone 1 has changed to Zone 2


Scenario: Done - delete zone
Given on Edit Table Layout with edit Zone
When touching the Zone 2 box
Then all zone has faded down
And touching the x on the upside with the right side
Then Are you sure to delete this zone? has pop-up
And select the OK button
Then Zone 2 has missing


Scenario: add a table
Given on Edit Table Layout
When touching the + button on the bottom side with the right side
Then the table button and the geometry button has moved from the right to the bottom side with the curve move

When touching the table button
Then the box of tables has moved from the right side to the left side
And select some table
Then the table you select has a show on the Table Layout


Scenario: Done - edit table
Given on Edit Table Layout
When touching the table you select
Then pop-up an X button, copy button, and i button
And touching the i button
Then the detail table has a pop-up
And touching the box on the top
Then can edit
And entering some number or some word
Then the box has change number to some number or some word

When touching the size button Small, Medium, and Large
Then the size button has changed to orange color when you touching

When touching the min seat drop-down
Then the Min seat drop-down has a drop-down to show 1-20
And select some number
Then show on the Min seat drop-down

When touching the max seat drop-down
Then the Max seat drop-down has a drop-down to show 1-20
And select some number
Then show on the Max seat drop-down
And touching the DONE button
Then the detail table has missed with save


Scenario: Cancel - edit table
Given on Edit Table Layout
When touching the table you select
Then pop-up an X button, copy button, and i button
And touching the i button
Then the detail table has a pop-up
And touching the box on the top
Then can edit
And entering some number or some word
Then the box has change number to some number or some word

When touching the size button Small, Medium, and Large
Then the size button has changed to orange color when you touching

When touching the min seat drop-down
Then the Min seat drop-down has a drop-down to show 1-20
And select some number
Then show on the Min seat drop-down

When touching the max seat drop-down
Then the Max seat drop-down has a drop-down to show 1-20
And select some number
Then show on the Max seat drop-down
And touching the CANCEL button
Then the detail table has missed with not save


Scenario: copy button on the table
When touching a table
Then pop-up an X button, copy button, and i button
And touching the copy button
Then a table has added 1 more


Scenario: X button success on the table
When touching a table
Then pop-up an X button, copy button, and i button
And touching the X button
Then Are you sure to delete this table? Has a pop-up with OK button & CANCEL button
And select the OK button
Then the table has missing

Scenario: Cancel the X button on the table
When touching a table
Then pop-up an X button, copy button, and i button
And touching the X button
Then Are you sure to delete this table? Has a pop-up with OK button & CANCEL button
And select the CANCEL button
Then the pop-up has missing


Scenario: edit geometry 
Given on Edit Table Layout
When touching the geometry button
Then the box of tables has changed to the box of geometry
And select some furniture
Then the furniture you select has a show on the Table Layout

When touching the geometry
Then shown up an X Button, copy button, expand button, and rotate button


Scenario: copy button on the table
When touching a table
Then pop-up an X button, copy button, and i button
And touching the copy button
Then furniture has added 1 more


Scenario: expand button on the table
When touching a table
Then pop-up an X button, copy button, and i button
And long touching the expand button and move left, right, up, down
Then furniture has expanded or shrink

Scenario: rotate button on the table
When touching a table
Then pop-up an X button, copy button, and i button
And long touching the rotate button and move left, right, up, down
Then furniture has to rotate


Scenario: X button success on the furniture
When touching a furniture
Then pop-up an X button, copy button, and i button
And touching the X button
Then Are you sure to delete this object? Has a pop-up with OK button & CANCEL button
And select the OK button
Then the furniture has missing

Scenario: cancel the X button on the furniture

When touching a furniture
Then pop-up an X button, copy button, and i button
And touching the X button
Then Are you sure to delete this object? Has a pop-up with OK button & CANCEL button
And select the CANCEL button
Then the pop-up has missing

When touching the CANCEL button on the top of the left side
Then a table or furniture you select has missing
