Feature: Edit Multi Menu
Scenario: Done - Edit Multi Menu with price
Given on Edit Menu page
When touching the pencil button
Then Edit Menu button, Move to category,  and Remove from category has a pop-up
And touching the Edit Menu button
Then Edit Menu page has changed to Edit Menu(Multi) page

When touching the Search textbox
Then can edit and the keyboard has moved up from the bottom side
And entering some word or number 
Then show the menu that is similar to the name entered
And fix it back to normal

When touching the Sort by Create Time drop-down
Then the Sort by Create Time drop-down has a drop-down to show Sort by Create Time, Sort by Name A-Z, Sort by Name Z-A, Sort by Category A-Z, Sort by Category A-Z, Sort by High Value, Sort by Low Value, and Sort by Menu ID
And select Sort by Name A-Z
Then menu changes the arrangement according to the name to A-Z

When touching the Sort by Create Time drop-down
Then the Sort by Create Time drop-down has a drop-down to show Sort by Create Time, Sort by Name A-Z, Sort by Name Z-A, Sort by Category A-Z, Sort by Category A-Z, Sort by High Value, Sort by Low Value, and Sort by Menu ID
And select Sort by Name Z-A
Then menu changes the arrangement according to the name to Z-A

When touching the Sort by Create Time drop-down
Then the Sort by Create Time drop-down has a drop-down to show Sort by Create Time, Sort by Name A-Z, Sort by Name Z-A, Sort by Category A-Z, Sort by Category A-Z, Sort by High Value, Sort by Low Value, and Sort by Menu ID
And select Sort by Category A-Z
Then menu changes the arrangement according to the category to A-Z

When touching the Sort by Create Time drop-down
Then the Sort by Create Time drop-down has a drop-down to show Sort by Create Time, Sort by Name A-Z, Sort by Name Z-A, Sort by Category A-Z, Sort by Category A-Z, Sort by High Value, Sort by Low Value, and Sort by Menu ID
And select Sort by Category Z-A
Then menu changes the arrangement according to the category to Z-A

When touching the Sort by Create Time drop-down
Then the Sort by Create Time drop-down has a drop-down to show Sort by Create Time, Sort by Name A-Z, Sort by Name Z-A, Sort by Category A-Z, Sort by Category A-Z, Sort by High Value, Sort by Low Value, and Sort by Menu ID
And select Sort by High Value
Then the menu changes the arrangement according to the High Value to the high-low

When touching the Sort by Create Time drop-down
Then the Sort by Create Time drop-down has a drop-down to show Sort by Create Time, Sort by Name A-Z, Sort by Name Z-A, Sort by Category A-Z, Sort by Category A-Z, Sort by High Value, Sort by Low Value, and Sort by Menu ID
And select Sort by Low Value
Then the menu changes the arrangement according to the Low Value to low-high

When touching the Sort by Create Time drop-down
Then the Sort by Create Time drop-down has a drop-down to show Sort by Create Time, Sort by Name A-Z, Sort by Name Z-A, Sort by Category A-Z, Sort by Category A-Z, Sort by High Value, Sort by Low Value, and Sort by Menu ID
And select Sort by Menu ID
Then menu changes the arrangement according to the Menu ID to A-Z form Menu ID
And fix it back to normal

When touching the Active button
Then show a menu in the category

When touching the Inactive button
Then show a menu not in a category
And fix it back to normal

Scenario: add a picture done form camera
Given on Edit Menu(Multi) page
When touching photo box
Then the Camera button, Library button, and Gallery button has a pop-up
And touching the Camera button
Then the Camera box has moved up from the bottom side
And touching the circle button on the center
Then show example picture with DONE button and CANCEL button
And select the DONE button 
Then show up on the photo box

Scenario: Cancel 1 - add a picture from the camera
Given on Edit Menu(Multi) page
When touching photo box
Then the Camera button, Library button, and Gallery button has a pop-up
And touching the Camera button
Then the Camera box has moved up from the bottom side
And touching the arrow button on the left side
Then the Camera box has missing

Scenario: Cancel 2 - add a picture from the camera
Given on Edit Menu(Multi) page
When touching photo box
Then the Camera button, Library button, and Gallery button has a pop-up
And touching the Camera button
Then the Camera box has moved up from the bottom side
And touching the circle button on the center
Then show example picture with DONE button and CANCEL button
And select the CANCEL button 
Then show up on photo box

Scenario: don’t allow photo form Camera with Don’t Allow
Given on Edit Menu(Multi) page
When touching photo box
Then the Camera button, Library button, and Gallery button has a pop-up
And touching the Camera button
Then Warning Please allow camera access from device setting has a pop-up with the OK button and “Wongnai POS” Would Like to Access the Camera. This app need to take photo from camera to upload menu picture has a pop-up on the Warning pop-up with OK button and Don’t Allow button
And touching a Don’t Allow button
Then “Wongnai POS” pop-up has missing
And touching the OK button on the Warning pop-up
Then Warning pop-up has missing
And touching the + box
Then the Camera button, Library button, and Gallery button has a pop-up
And touching the Camera button
Then Warning Please allow camera access from device setting has a pop-up with the OK button
And touching the OK button and go to setting of your device
Then Scroll down and Select Wongnai POS
And touching the Camera toggle and move to Wongnai POS app
Then PIN and go to Add Menu page
And touching photo box
Then the Camera button, Library button, and Gallery button has a pop-up
And touching the Camera button
Then the Camera box has moved up from the bottom side


Scenario: add a photo from Library done
Given on Edit Menu(Multi) page
When touching photo box
Then the Camera button, Library button, and Gallery button has a pop-up
And select the Library button
Then button pop-up has changed to Photo pop-up with All Photos, Recents, and album you created
And select some album
Then album changed to photo in the album you select
And select some photo
Then the photo box has moved up from the bottom side with the DONE button, and CANCEL
And select the DONE button
Then the photo box has missing and photo you select has shown up on photo box


Scenario: Cancel - add a photo from Library
Given on Edit Menu(Multi) page
When touching photo box
Then the Camera button, Library button, and Gallery button has a pop-up
And select the Library button
Then button pop-up has changed to Photo pop-up with All Photos, Recents, and album you created
And select some album
Then album changed to photo in the album you select
And select some photo
Then the photo box has moved up from the bottom side with the DONE button, and CANCEL
And select the CANCEl button
Then the photo box has missed and no photo on photo box


Scenario: add data
Given on Edit Menu(Multi) page
When touching the Menu name 1
Then can edit and the keyboard has faded up from the bottom side
And entering or delete some word or number in the Menu name 1 textbox
Then some word or number has shown up on the Menu name 1 textbox 

When touching the  Menu name 2
Then can edit and the keyboard has faded up from the bottom side
And entering or delete some word or number in the Menu name 2 textbox
Then some word or number has shown up on the Menu name 2 textbox 

When touching the price textbox
Then can edit and the keyboard has faded up from the bottom side
And entering or delete some number in the price textbox
Then some number has shown up on the price textbox

When touching the Non-VAT checkbox 
Then the green checkmark has change to grey color on the Non-VAT checkbox 

When touching the Menu ID textbox
Then can edit and the keyboard has faded up from the bottom side
And entering or delete some word and number in the Menu ID textbox 
Then some word and number has shown up on the Menu ID textbox

When touching the Delivery button 
Then the Delivery button has changed to green color from the grey color

When touching the Recommend button
Then the Recommend button has changed to grey color from the light blue color


Scenario: Done - Edit Menu(Multi) with price
Given on Edit Menu(Multi) page
When touching the DONE button
Then toast Saving... has shown up and when saved toast has missing


Scenario: Cancel - Edit Menu(Multi) with price
Given on Edit Menu(Multi) page
When touching the CANCEL button
Then Quick add page has moved down with missing


Scenario: Done - Edit Menu(Multi) with variable price
Given on Edit Menu(Multi) page
When touching the paper button on the top of the right side
Then Single add button and Quick add button has a pop-up

When touching the Quick add button
Then the Add Menu page has moved up from the bottom side


Scenario: add a picture done form camera
Given on Edit Menu(Multi) page
When touching photo box
Then the Camera button, Library button, and Gallery button has a pop-up
And touching the Camera button
Then the Camera box has moved up from the bottom side
And touching the circle button on the center
Then show example picture with DONE button and CANCEL button
And select the DONE button 
Then show up on the photo box

Scenario: Cancel 1 - add a picture from the camera
Given on Edit Menu(Multi) page
When touching photo box
Then the Camera button, Library button, and Gallery button has a pop-up
And touching the Camera button
Then the Camera box has moved up from the bottom side
And touching the arrow button on the left side
Then the Camera box has missing

Scenario: Cancel 2 - add a picture from the camera
Given on Edit Menu(Multi) page
When touching photo box
Then the Camera button, Library button, and Gallery button has a pop-up
And touching the Camera button
Then the Camera box has moved up from the bottom side
And touching the circle button on the center
Then show example picture with DONE button and CANCEL button
And select the CANCEL button 
Then show up on photo box

Scenario: don’t allow photo form Camera with Don’t Allow
Given on Edit Menu(Multi) page
When touching photo box
Then the Camera button, Library button, and Gallery button has a pop-up
And touching the Camera button
Then Warning Please allow camera access from device setting has a pop-up with the OK button and “Wongnai POS” Would Like to Access the Camera. This app need to take photo from camera to upload menu picture has a pop-up on the Warning pop-up with OK button and Don’t Allow button
And touching a Don’t Allow button
Then “Wongnai POS” pop-up has missing
And touching the OK button on the Warning pop-up
Then Warning pop-up has missing
And touching the + box
Then the Camera button, Library button, and Gallery button has a pop-up
And touching the Camera button
Then Warning Please allow camera access from device setting has a pop-up with the OK button
And touching the OK button and go to setting of your device
Then Scroll down and Select Wongnai POS
And touching the Camera toggle and move to Wongnai POS app
Then PIN and go to Add Menu page
And touching photo box
Then the Camera button, Library button, and Gallery button has a pop-up
And touching the Camera button
Then the Camera box has moved up from the bottom side


Scenario: add a photo from Library done
Given on Edit Menu(Multi) page
When touching photo box
Then the Camera button, Library button, and Gallery button has a pop-up
And select the Library button
Then button pop-up has changed to Photo pop-up with All Photos, Recents, and album you created
And select some album
Then album changed to photo in the album you select
And select some photo
Then the photo box has moved up from the bottom side with the DONE button, and CANCEL
And select the DONE button
Then the photo box has missing and photo you select has shown up on photo box


Scenario: Cancel - add a photo from Library
Given on Edit Menu(Multi) page
When touching photo box
Then the Camera button, Library button, and Gallery button has a pop-up
And select the Library button
Then button pop-up has changed to Photo pop-up with All Photos, Recents, and album you created
And select some album
Then album changed to photo in the album you select
And select some photo
Then the photo box has moved up from the bottom side with the DONE button, and CANCEL
And select the CANCEl button
Then the photo box has missed and no photo on photo box


Scenario: add data
Given on Add Menu
When touching the Menu name 1
Then can edit and the keyboard has faded up from the bottom side
And entering or delete some word or number in the Menu name 1 textbox
Then some word or number has shown up on the Menu name 1 textbox 

When touching the  Menu name 2
Then can edit and the keyboard has faded up from the bottom side
And entering or delete some word or number in the Menu name 2 textbox
Then some word or number has shown up on the Menu name 2 textbox 

When touching the Variable checkbox
Then green checkmark has shown on the Variable checkbox

When touching the Non-VAT checkbox 
Then the green checkmark has on the Non-VAT checkbox 

When touching the Menu ID textbox
Then can edit and the keyboard has faded up from the bottom side
And entering some word and number in the Menu ID textbox 
Then some word and number has shown up on the Menu ID textbox

When touching the Delivery button 
Then the Delivery button has changed to grey color from the green color

When touching the Recommend button
Then the Recommend button has changed to light blue color from the grey color

When touching the DONE button
Then toast Saving... has shown up and Warning Variable menu can not enable delivery has a pop-up with OK button
And touching the OK button
Then Warning pop-up has missing
And touching the Delivery button
Then the Delivery button has changed to grey color

When touching the DONE button
Then toast Saving... has shown up and when saved toast has missing


Scenario: Cancel - Quick add with price
Given on Quick add page
When touching the CANCEL button
Then Quick add page has moved down with missing


