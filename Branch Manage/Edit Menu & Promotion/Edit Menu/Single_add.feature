Feature: Single add
Scenario: Single add with price
Given on Edit Menu page
When touching the paper button on the top of the right side
Then Single add button and Quick add button has a pop-up

When touching the Single add button
Then the Add Menu page has moved up from the bottom side


Scenario: allow photo form Camera with OK button
When touching the + box
Then the Camera button, Library button, and Gallery button has a pop-up
And touching the Camera button
Then Warning Please allow camera access from device setting has a pop-up with the OK button and “Wongnai POS” Would Like to Access the Camera. This app need to take photo from camera to upload menu picture has a pop-up on the Warning pop-up with OK button and Don’t Allow button
And touching an OK button
Then “Wongnai POS” pop-up has missing
And touching the OK button on the Warning pop-up
Then Warning pop-up has missing


Scenario: add a picture done form camera
Given on Add Menu
When touching the + box
Then the Camera button, Library button, and Gallery button has a pop-up
And touching the Camera button
Then the Camera box has moved up from the bottom side
And touching the circle button on the center
Then show example picture with DONE button and CANCEL button
And select the DONE button 
Then show up on the + box


Scenario: Cancel 1 - add a picture from the camera
Given on Add Menu
When touching the + box
Then the Camera button, Library button, and Gallery button has a pop-up
And touching the Camera button
Then the Camera box has moved up from the bottom side
And touching the arrow button on the left side
Then the Camera box has missing


Scenario: Cancel 2 - add a picture from the camera
Given on Add Menu
When touching the + box
Then the Camera button, Library button, and Gallery button has a pop-up
And touching the Camera button
Then the Camera box has moved up from the bottom side
And touching the circle button on the center
Then show example picture with DONE button and CANCEL button
And select the CANCEL button 
Then show up on the + box


Scenario: don’t allow photo form Camera with Don’t Allow
When touching the + box
Then the Camera button, Library button, and Gallery button has a pop-up
And touching the Camera button
Then Warning Please allow camera access from device setting has a pop-up with the OK button and “Wongnai POS” Would Like to Access the Camera. This app need to take photo from camera to upload menu picture has a pop-up on the Warning pop-up with OK button and Don’t Allow button
And touching a Don’t Allow button
Then “Wongnai POS” pop-up has missing
And touching the OK button on the Warning pop-up
Then Warning pop-up has missing
And touching the + box
Then the Camera button, Library button, and Gallery button has a pop-up
And touching the Camera button
Then Warning Please allow camera access from device setting has a pop-up with the OK button
And touching the OK button and go to setting of your device
Then Scroll down and Select Wongnai POS
And touching the Camera toggle and move to Wongnai POS app
Then PIN and go to Add Menu page
And touching the + box
Then the Camera button, Library button, and Gallery button has a pop-up
And touching the Camera button
Then the Camera box has moved up from the bottom side


Scenario: add a photo from Library done
Given on Add Menu page
When touching the + box
Then the Camera button, Library button, and Gallery button has a pop-up
And select the Library button
Then button pop-up has changed to Photo pop-up with All Photos, Recents, and album you created
And select some album
Then album changed to photo in the album you select
And select some photo
Then the photo box has moved up from the bottom side with the DONE button, and CANCEL
And select the DONE button
Then the photo box has missing and photo you select has shown up on + box


Scenario: Cancel - add a photo from Library
Given on Add Menu page
When touching the + box
Then the Camera button, Library button, and Gallery button has a pop-up
And select the Library button
Then button pop-up has changed to Photo pop-up with All Photos, Recents, and album you created
And select some album
Then album changed to photo in the album you select
And select some photo
Then the photo box has moved up from the bottom side with the DONE button, and CANCEL
And select the CANCEl button
Then the photo box has missed and no photo on the + box


Scenario: add data
Given on Add Menu
When touching the Menu name 1
Then can edit and the keyboard has faded up from the bottom side
And entering some word or number in the Menu name 1 textbox
Then some word or number has shown up on the Menu name 1 textbox 

When touching the  Menu name 2
Then can edit and the keyboard has faded up from the bottom side
And entering some word or number in the Menu name 2 textbox
Then some word or number has shown up on the Menu name 2 textbox 

When touching the price textbox
Then can edit and the keyboard has faded up from the bottom side
And entering some number in the price textbox
Then some number has shown up on the price textbox

When touching the Non-VAT checkbox 
Then the green checkmark has on the Non-VAT checkbox 

When touching the Menu ID textbox
Then can edit and the keyboard has faded up from the bottom side
And entering some word and number in the Menu ID textbox 
Then some word and number has shown up on the Menu ID textbox


Scenario: entering Barcode textbox by keyboard
Given on Add Menu
When touching the Barcode textbox
Then can edit and the keyboard has faded up from the bottom side
And entering some word and number in the Barcode textbox
Then some word and number has shown up on the Barcode textbox


Scenario: allow photo form Camera with OK button
When touching the camera button between the Barcode textbox
Then Warning Please allow camera access from device setting has a pop-up with the OK button and “Wongnai POS” Would Like to Access the Camera. This app need to take photo from camera to upload menu picture has a pop-up on the Warning pop-up with OK button and Don’t Allow button
And touching an OK button
Then “Wongnai POS” pop-up has missing
And touching the OK button on the Warning pop-up
Then Warning pop-up has missing


Scenario: add a barcode done form camera
Given on Add Menu
When touching the camera button between the Barcode textbox
Then the Camera box has moved up from the bottom side
And moved the iPad to some barcode
Then show the text and number from the barcode you scan has shown up on the Barcode textbox


Scenario: Cancel 1 - add a picture from the Camera
Given on Add Menu
When touching the camera button between the Barcode textbox
Then the Camera box has moved up from the bottom side
And touching the arrow button on the left side
Then the Camera box has missing


Scenario: Cancel 2 - add a picture from the Camera
Given on Add Menu
When touching the camera button between the Barcode textbox
Then the Camera box has moved up from the bottom side
And touching the circle button on the center
Then show example picture with DONE button and CANCEL button
And select the CANCEL button 
Then show up on the + box


Scenario: don’t allow photo form Camera with Don’t Allow
When touching the camera button between the Barcode textbox
Then Warning Please allow camera access from device setting has a pop-up with the OK button and “Wongnai POS” Would Like to Access the Camera. This app need to take photo from camera to upload menu picture has a pop-up on the Warning pop-up with OK button and Don’t Allow button
And touching a Don’t Allow button
Then “Wongnai POS” pop-up has missing
And touching the OK button on the Warning pop-up
Then Warning pop-up has missing
And touching the camera button between the Barcode textbox
Then Warning Please allow camera access from device setting has a pop-up with the OK button
And touching the OK button and go to setting of your device
Then Scroll down and Select Wongnai POS
And touching the Camera toggle and move to Wongnai POS app
Then PIN and go to Add Menu page
And touching the camera button between the Barcode textbox
Then the Camera box has moved up from the bottom side

When touching the Quick mode color box
Then  a color box has faded from the left side to the right
And select some color
Then color you select has a show on the Quick mode color box

When touching the Menu Descriptions textbox
Then can edit and the keyboard has faded up from the bottom side
And entering some word or number in the Menu Descriptions textbox
Then some word or number has shown up on the Menu Descriptions textbox 

When touching the Recommend button
Then the Recommend button has changed to light blue color from the grey color

When touching the Seasonal button
Then the Seasonal button has changed to orange color from the grey color

When touching the Take time button
Then the Take time button has changed to yellow color from the grey color

When touching the Delivery button 
Then the Delivery button has changed to grey color from the green color

When touching the Out of stock button
Then the Out of stock button has changed to red color from the grey color

When touching some Category in the Assign to Category box
Then green checkmark has shown up on the back of the row of the category you select

When touching the Option button
Then the Option button has changed to orange color and option page has moved up from the bottom side


Scenario: Done - create option
Given on the Create Option Group pop-up
When touching the CREATE NEW button
Then the Create Option Group pop-up has moved up from the bottom side
And touching the CREATE NEW CHOICE button
Then the Create Option Group pop-up has faded to the Edit Option from the right side to the left side

When touching the Option name textbox
Then can edit and the keyboard has faded up from the bottom side
And entering some word or number in the Option name textbox
Then some word or number has shown up on the Option name textbox

When touching the Extra cost textbox
Then can edit and the keyboard has faded up fo from the bottom side
And entering some number in h the Extra cost textbox
Then some number has shown up on the Extra cost textbox

When touching the DONE button
Then toast Saving… has a pop-up and missing when saved and pop-up Edit Option has faded to Create Option Group pop-up from the left side to the right side with show up option you create under the Assign to Choices


Scenario: Cancel - create an option 
Given on Edit Option pop-up
When touching the CANCEL button
Then pop-up Edit Option has faded to Create Option Group pop-up from the left side to the right side with not show up option you create under the Assign to Choices


Scenario: delete option
Given on Create Option Group pop-up
When touching with slide right or left side on an option you create
Then pencil button on the backside of the option you slide has changed to the X button
And touching the X button
Then toast Loading… has shown up and when delete done toast has missed with the option you select


Scenario: Done - Edit Option
Given on Create Option Group pop-up
When touching the pencil button
Then Create Option Group pop-up has changed to Edit Option pop-up

When touching the Option name textbox
Then can edit and the keyboard has moved up from the bottom side
And entering some word or number
Then show on the Option name textbox

When touching the Extra cost textbox
Then can edit and the keyboard has moved up from the bottom side
And entering some number
Then show on the Extra cost textbox

When touching the DONE button
Then toast Saving… has a pop-up and missing when saved and pop-up Edit Option has faded to Create Option Group pop-up from the left side to the right side with show up option you create under the Assign to Choices


Scenario: Cancel - edit an option 
Given on Edit Option pop-up
When touching the CANCEL button
Then pop-up Edit Option has faded to Create Option Group pop-up from the left side to the right side with not show up option you create under the Assign to Choices


Scenario: Create Option Group has done with not Required & Max Qty
When touching Option group name textbox
Then can edit and the keyboard has moved up from the bottom side
And  entering some word or number
Then show on the Option group name textbox

When touching the Next button
Then Create Option Group pop-up has changed to confirm Create Option Group pop-up
And touching the DONE button
Then Saving… has a pop-up and when saved option group has shown up on the right side of the option page


Scenario: Done - Create Option Group with required
When touching Option group name textbox
Then can edit and the keyboard has moved up from the bottom side
And  entering some word or number
Then show on the Option group name textbox

When touching Required toggle
Then Required toggle has moved to the right side from the left side

When touching the Max Qty textbox
Then can edit and the keyboard has moved up from the bottom side
And entering some number
Then show on the Max Qty textbox

When touching the Next button
Then Create Option Group pop-up has changed to confirm Create Option Group pop-up
And touching the DONE button
Then Saving… has a pop-up and when saved option group has shown up on the right side of the option page


Scenario: Cancel 1 - Option Group
When touching Option group name textbox
Then can edit and the keyboard has moved up from the bottom side
And  entering some word or number
Then show on the Option group name textbox

When touching the CANCEL button
Then Create Option Group has moved down and  missing


Scenario: Cancel 2 - Option Group
When touching Option group name textbox
Then can edit and the keyboard has moved up from the bottom side
And  entering some word or number
Then show on the Option group name textbox

When touching the Next button
Then Create Option Group pop-up has changed to confirm Create Option Group pop-up
And touching the Arrow button on the top of the left side
Then the confirm Create Option Group pop-up has changed to the Create Option Group pop-up from the right to the left side 
And touching the CANCEL button
Then Create Option Group has moved down and  missing


Scenario: edit Option Group has done with not Required & Max Qty
When touching the pencil button on the Option Group
Then the Edit Option Group pop-up has moved up
And touching the Option Group name textbox
Then can edit and the keyboard has moved up from the bottom side

When touching the Next button
Then Create Option Group pop-up has changed to confirm Create Option Group pop-up
And touching the DONE button
Then Saving… has a pop-up and when saved option group has shown up on the right side of the option page


Scenario: Done - Edit Option Group with required
When touching the pencil button on the Option Group
Then the Edit Option Group pop-up has moved up
And touching the Option Group name textbox
Then can edit and the keyboard has moved up from the bottom side

When touching Required toggle
Then Required toggle has moved to the right side from the left side

When touching the Max Qty textbox
Then can edit and the keyboard has moved up from the bottom side
And entering some number
Then show on the Max Qty textbox

When touching the Next button
Then Create Option Group pop-up has changed to confirm Create Option Group pop-up
And touching the DONE button
Then Saving… has a pop-up and when saved option group has shown up on the right side of the option page


Scenario: Cancel 1 - edit Option Group
When touching the pencil button on the Option Group
Then the Edit Option Group pop-up has moved up
And touching the Option Group name textbox
Then can edit and the keyboard has moved up from the bottom side

When touching the CANCEL button
Then Create Option Group has moved down and  missing


Scenario: Cancel 2 - Edit Option Group
When touching the pencil button on the Option Group
Then the Edit Option Group pop-up has moved up
And touching the Option Group name textbox
Then can edit and the keyboard has moved up from the bottom side

When touching the Next button
Then Create Option Group pop-up has changed to confirm Create Option Group pop-up
And touching the Arrow button on the top of the left side
Then the confirm Create Option Group pop-up has changed to the Create Option Group pop-up from the right to the left side 
And touching the CANCEL button
Then Create Option Group has moved down and  missing


Scenario: delete Option Group
Given on Option page
When touching with a slide right or left side on the pencil button on the Option Group
Then the pencil button has changed to the X button
And the Option Group has missing


Scenario: Select Option to active
Given on Option page
When touching the arrow on the Option group
Then the  arrow on the Option Group has changed to checkmark and the Option Group you select has shown on the left of the Option page


Scenario: Unselect Option to unactive
Given on Option page
When touching the X button on the Option Group active
Then the Option Group you select has missed with the checkmark on the Option Group has changed to the arrow


Scenario: Done - create an Ingredient
Given on ingredient page
When touching the CREATE NEW button
Then the left side has changed to grey and the right side has changed to Create Ingredient page from the right side to the left side

When touching the Ingredient name textbox
Then can edit and the keyboard has moved up from the bottom side
And entering some word or number
Then some word or number has shown on the Ingredient name textbox
 
When touching the DONE button
Then toast Saving… has a pop-up and when saved Create Ingredient page has changed to Ingredient page with the ingredient you created


Scenario: Done - Edit Ingredient
Given on Ingredient page
When touching the pencil button on the Ingredient you select
Then the left side has changed to grey and the right side has changed to Edit Ingredient
And touching the Ingredient name textbox
Then can edit and the keyboard has moved up
And entering some word or number
Then some word or number has shown on the Ingredient name

When touching the DONE button
Then toast Saving… has shown up and when saved toast has missed with Edit Ingredient page has changed to Ingredient page


Scenario: Cancel - edit Ingredient
Given on Ingredient page
When touching the pencil button on the Ingredient you select
Then the left side has changed to grey and the right side has changed to Edit Ingredient
And touching the Ingredient name textbox
Then can edit and the keyboard has moved up
And entering some word or number
Then some word or number has shown on the Ingredient name

When touching the CANCEL button
Then Edit Ingredient page has changed to Ingredient page

Scenario: Done - Single add with price
Given on Ingredient page
When touching the Ingredient
Then Ingredient page has moved down to Add Menu page
And touching the DONE button
Then pop-up Updating... has a pop-up and when updated pop-up has missing


Scenario: Cancel - Single add with price
Given on Ingredient page
When touching the Ingredient
Then Ingredient page has moved down to Add Menu page
And touching the CANCEL button
Then Add Menu page has moved down with missing


Scenario: Done - Single add with variable price
Given on Edit Menu page
When touching the paper button on the top of the right side
Then Single add button and Quick add button has a pop-up

When touching the Quick add button
Then the Add Menu page has moved up from the bottom side


Scenario: allow photo form Camera with OK button
When touching the + box
Then the Camera button, Library button, and Gallery button has a pop-up
And touching the Camera button
Then Warning Please allow camera access from device setting has a pop-up with the OK button and “Wongnai POS” Would Like to Access the Camera. This app need to take photo from camera to upload menu picture has a pop-up on the Warning pop-up with OK button and Don’t Allow button
And touching an OK button
Then “Wongnai POS” pop-up has missing
And touching the OK button on the Warning pop-up
Then Warning pop-up has missing


Scenario: add a picture done form camera
Given on Add Menu
When touching the + box
Then the Camera button, Library button, and Gallery button has a pop-up
And touching the Camera button
Then the Camera box has moved up from the bottom side
And touching the circle button on the center
Then show example picture with DONE button and CANCEL button
And select the DONE button 
Then show up on the + box


Scenario: Cancel 1 - add a picture from the camera
Given on Add Menu
When touching the + box
Then the Camera button, Library button, and Gallery button has a pop-up
And touching the Camera button
Then the Camera box has moved up from the bottom side
And touching the arrow button on the left side
Then the Camera box has missing


Scenario: Cancel 2 - add a picture from the camera
Given on Add Menu
When touching the + box
Then the Camera button, Library button, and Gallery button has a pop-up
And touching the Camera button
Then the Camera box has moved up from the bottom side
And touching the circle button on the center
Then show example picture with DONE button and CANCEL button
And select the CANCEL button 
Then show up on the + box


Scenario: don’t allow photo form Camera with Don’t Allow
When touching the + box
Then the Camera button, Library button, and Gallery button has a pop-up
And touching the Camera button
Then Warning Please allow camera access from device setting has a pop-up with the OK button and “Wongnai POS” Would Like to Access the Camera. This app need to take photo from camera to upload menu picture has a pop-up on the Warning pop-up with OK button and Don’t Allow button
And touching a Don’t Allow button
Then “Wongnai POS” pop-up has missing
And touching the OK button on the Warning pop-up
Then Warning pop-up has missing
And touching the + box
Then the Camera button, Library button, and Gallery button has a pop-up
And touching the Camera button
Then Warning Please allow camera access from device setting has a pop-up with the OK button
And touching the OK button and go to setting of your device
Then Scroll down and Select Wongnai POS
And touching the Camera toggle and move to Wongnai POS app
Then PIN and go to Add Menu page
And touching the + box
Then the Camera button, Library button, and Gallery button has a pop-up
And touching the Camera button
Then the Camera box has moved up from the bottom side


Scenario: add a photo from Library done
Given on Add Menu page
When touching the + box
Then the Camera button, Library button, and Gallery button has a pop-up
And select the Library button
Then button pop-up has changed to Photo pop-up with All Photos, Recents, and album you created
And select some album
Then album changed to photo in the album you select
And select some photo
Then the photo box has moved up from the bottom side with the DONE button, and CANCEL
And select the DONE button
Then the photo box has missing and photo you select has shown up on + box


Scenario: Cancel - add a photo from Library
Given on Add Menu page
When touching the + box
Then the Camera button, Library button, and Gallery button has a pop-up
And select the Library button
Then button pop-up has changed to Photo pop-up with All Photos, Recents, and album you created
And select some album
Then album changed to photo in the album you select
And select some photo
Then the photo box has moved up from the bottom side with the DONE button, and CANCEL
And select the CANCEl button
Then the photo box has missed and no photo on the + box


Scenario: add data
Given on Add Menu
When touching the Menu name 1
Then can edit and the keyboard has faded up from the bottom side
And entering some word or number in the Menu name 1 textbox
Then some word or number has shown up on the Menu name 1 textbox 

When touching the  Menu name 2
Then can edit and the keyboard has faded up from the bottom side
And entering some word or number in the Menu name 2 textbox
Then some word or number has shown up on the Menu name 2 textbox 

When touching the Variable checkbox
Then green checkmark has shown on the Variable checkbox

When touching the Non-VAT checkbox 
Then the green checkmark has on the Non-VAT checkbox 

When touching the Menu ID textbox
Then can edit and the keyboard has faded up from the bottom side
And entering some word and number in the Menu ID textbox 
Then some word and number has shown up on the Menu ID textbox


Scenario: entering Barcode textbox by keyboard
Given on Add Menu
When touching the Barcode textbox
Then can edit and the keyboard has faded up from the bottom side
And entering some word and number in the Barcode textbox
Then some word and number has shown up on the Barcode textbox


Scenario: allow photo form Camera with OK button
When touching the camera button between the Barcode textbox
Then Warning Please allow camera access from device setting has a pop-up with the OK button and “Wongnai POS” Would Like to Access the Camera. This app need to take photo from camera to upload menu picture has a pop-up on the Warning pop-up with OK button and Don’t Allow button
And touching an OK button
Then “Wongnai POS” pop-up has missing
And touching the OK button on the Warning pop-up
Then Warning pop-up has missing


Scenario: add a barcode done form camera
Given on Add Menu
When touching the camera button between the Barcode textbox
Then the Camera box has moved up from the bottom side
And moved the iPad to some barcode
Then show the text and number from the barcode you scan has shown up on the Barcode textbox


Scenario: Cancel 1 - add a picture from the Camera
Given on Add Menu
When touching the camera button between the Barcode textbox
Then the Camera box has moved up from the bottom side
And touching the arrow button on the left side
Then the Camera box has missing


Scenario: Cancel 2 - add a picture from the Camera
Given on Add Menu
When touching the camera button between the Barcode textbox
Then the Camera box has moved up from the bottom side
And touching the circle button on the center
Then show example picture with DONE button and CANCEL button
And select the CANCEL button 
Then show up on the + box


Scenario: don’t allow photo form Camera with Don’t Allow
When touching the camera button between the Barcode textbox
Then Warning Please allow camera access from device setting has a pop-up with the OK button and “Wongnai POS” Would Like to Access the Camera. This app need to take photo from camera to upload menu picture has a pop-up on the Warning pop-up with OK button and Don’t Allow button
And touching a Don’t Allow button
Then “Wongnai POS” pop-up has missing
And touching the OK button on the Warning pop-up
Then Warning pop-up has missing
And touching the camera button between the Barcode textbox
Then Warning Please allow camera access from device setting has a pop-up with the OK button
And touching the OK button and go to setting of your device
Then Scroll down and Select Wongnai POS
And touching the Camera toggle and move to Wongnai POS app
Then PIN and go to Add Menu page
And touching the camera button between the Barcode textbox
Then the Camera box has moved up from the bottom side

When touching the Quick mode color box
Then  a color box has faded from the left side to the right
And select some color
Then color you select has a show on the Quick mode color box

When touching the Menu Descriptions textbox
Then can edit and the keyboard has faded up from the bottom side
And entering some word or number in the Menu Descriptions textbox
Then some word or number has shown up on the Menu Descriptions textbox 

When touching the Recommend button
Then the Recommend button has changed to light blue color from the grey color

When touching the Seasonal button
Then the Seasonal button has changed to orange color from the grey color

When touching the Take time button
Then the Take time button has changed to yellow color from the grey color

When touching the Delivery button 
Then the Delivery button has changed to grey color from the green color

When touching the Out of stock button
Then the Out of stock button has changed to red color from the grey color

When touching some Category in the Assign to Category box
Then green checkmark has shown up on the back of the row of the category you select

When touching the Option button
Then the Option button has changed to orange color and option page has moved up from the bottom side


Scenario: Done - create option
Given on the Create Option Group pop-up
When touching the CREATE NEW button
Then the Create Option Group pop-up has moved up from the bottom side
And touching the CREATE NEW CHOICE button
Then the Create Option Group pop-up has faded to the Edit Option from the right side to the left side

When touching the Option name textbox
Then can edit and the keyboard has faded up from the bottom side
And entering some word or number in the Option name textbox
Then some word or number has shown up on the Option name textbox

When touching the Extra cost textbox
Then can edit and the keyboard has faded up fo from the bottom side
And entering some number in h the Extra cost textbox
Then some number has shown up on the Extra cost textbox

When touching the DONE button
Then toast Saving… has a pop-up and missing when saved and pop-up Edit Option has faded to Create Option Group pop-up from the left side to the right side with show up option you create under the Assign to Choices


Scenario: Cancel - create an option 
Given on Edit Option pop-up
When touching the CANCEL button
Then pop-up Edit Option has faded to Create Option Group pop-up from the left side to the right side with not show up option you create under the Assign to Choices


Scenario: delete option
Given on Create Option Group pop-up
When touching with slide right or left side on an option you create
Then pencil button on the backside of the option you slide has changed to the X button
And touching the X button
Then toast Loading… has shown up and when delete done toast has missed with the option you select


Scenario: Done - Edit Option
Given on Create Option Group pop-up
When touching the pencil button
Then Create Option Group pop-up has changed to Edit Option pop-up

When touching the Option name textbox
Then can edit and the keyboard has moved up from the bottom side
And entering some word or number
Then show on the Option name textbox

When touching the Extra cost textbox
Then can edit and the keyboard has moved up from the bottom side
And entering some number
Then show on the Extra cost textbox

When touching the DONE button
Then toast Saving… has a pop-up and missing when saved and pop-up Edit Option has faded to Create Option Group pop-up from the left side to the right side with show up option you create under the Assign to Choices


Scenario: Cancel - edit an option 
Given on Edit Option pop-up
When touching the CANCEL button
Then pop-up Edit Option has faded to Create Option Group pop-up from the left side to the right side with not show up option you create under the Assign to Choices


Scenario: Create Option Group has done with not Required & Max Qty
When touching Option group name textbox
Then can edit and the keyboard has moved up from the bottom side
And  entering some word or number
Then show on the Option group name textbox

When touching the Next button
Then Create Option Group pop-up has changed to confirm Create Option Group pop-up
And touching the DONE button
Then Saving… has a pop-up and when saved option group has shown up on the right side of the option page


Scenario: Done - Create Option Group with required
When touching Option group name textbox
Then can edit and the keyboard has moved up from the bottom side
And  entering some word or number
Then show on the Option group name textbox

When touching Required toggle
Then Required toggle has moved to the right side from the left side

When touching the Max Qty textbox
Then can edit and the keyboard has moved up from the bottom side
And entering some number
Then show on the Max Qty textbox

When touching the Next button
Then Create Option Group pop-up has changed to confirm Create Option Group pop-up
And touching the DONE button
Then Saving… has a pop-up and when saved option group has shown up on the right side of the option page


Scenario: Cancel 1 - Option Group
When touching Option group name textbox
Then can edit and the keyboard has moved up from the bottom side
And  entering some word or number
Then show on the Option group name textbox

When touching the CANCEL button
Then Create Option Group has moved down and  missing


Scenario: Cancel 2 - Option Group
When touching Option group name textbox
Then can edit and the keyboard has moved up from the bottom side
And  entering some word or number
Then show on the Option group name textbox

When touching the Next button
Then Create Option Group pop-up has changed to confirm Create Option Group pop-up
And touching the Arrow button on the top of the left side
Then the confirm Create Option Group pop-up has changed to the Create Option Group pop-up from the right to the left side 
And touching the CANCEL button
Then Create Option Group has moved down and  missing


Scenario: edit Option Group has done with not Required & Max Qty
When touching the pencil button on the Option Group
Then the Edit Option Group pop-up has moved up
And touching the Option Group name textbox
Then can edit and the keyboard has moved up from the bottom side

When touching the Next button
Then Create Option Group pop-up has changed to confirm Create Option Group pop-up
And touching the DONE button
Then Saving… has a pop-up and when saved option group has shown up on the right side of the option page


Scenario: Done - Edit Option Group with required
When touching the pencil button on the Option Group
Then the Edit Option Group pop-up has moved up
And touching the Option Group name textbox
Then can edit and the keyboard has moved up from the bottom side

When touching Required toggle
Then Required toggle has moved to the right side from the left side

When touching the Max Qty textbox
Then can edit and the keyboard has moved up from the bottom side
And entering some number
Then show on the Max Qty textbox

When touching the Next button
Then Create Option Group pop-up has changed to confirm Create Option Group pop-up
And touching the DONE button
Then Saving… has a pop-up and when saved option group has shown up on the right side of the option page


Scenario: Cancel 1 - edit Option Group
When touching the pencil button on the Option Group
Then the Edit Option Group pop-up has moved up
And touching the Option Group name textbox
Then can edit and the keyboard has moved up from the bottom side

When touching the CANCEL button
Then Create Option Group has moved down and  missing


Scenario: Cancel 2 - Edit Option Group
When touching the pencil button on the Option Group
Then the Edit Option Group pop-up has moved up
And touching the Option Group name textbox
Then can edit and the keyboard has moved up from the bottom side

When touching the Next button
Then Create Option Group pop-up has changed to confirm Create Option Group pop-up
And touching the Arrow button on the top of the left side
Then the confirm Create Option Group pop-up has changed to the Create Option Group pop-up from the right to the left side 
And touching the CANCEL button
Then Create Option Group has moved down and  missing


Scenario: delete Option Group
Given on Option page
When touching with a slide right or left side on the pencil button on the Option Group
Then the pencil button has changed to the X button
And the Option Group has missing


Scenario: Select Option to active
Given on Option page
When touching the arrow on the Option group
Then the  arrow on the Option Group has changed to checkmark and the Option Group you select has shown on the left of the Option page


Scenario: Unselect Option to unactive
Given on Option page
When touching the X button on the Option Group active
Then the Option Group you select has missed with the checkmark on the Option Group has changed to the arrow


Scenario: Done - create an Ingredient
Given on ingredient page
When touching the CREATE NEW button
Then the left side has changed to grey and the right side has changed to Create Ingredient page from the right side to the left side

When touching the Ingredient name textbox
Then can edit and the keyboard has moved up from the bottom side
And entering some word or number
Then some word or number has shown on the Ingredient name textbox
 
When touching the DONE button
Then toast Saving… has a pop-up and when saved Create Ingredient page has changed to Ingredient page with the ingredient you created


Scenario: Done - Edit Ingredient
Given on Ingredient page
When touching the pencil button on the Ingredient you select
Then the left side has changed to grey and the right side has changed to Edit Ingredient
And touching the Ingredient name textbox
Then can edit and the keyboard has moved up
And entering some word or number
Then some word or number has shown on the Ingredient name

When touching the DONE button
Then toast Saving… has shown up and when saved toast has missed with Edit Ingredient page has changed to Ingredient page


Scenario: Cancel - edit Ingredient
Given on Ingredient page
When touching the pencil button on the Ingredient you select
Then the left side has changed to grey and the right side has changed to Edit Ingredient
And touching the Ingredient name textbox
Then can edit and the keyboard has moved up
And entering some word or number
Then some word or number has shown on the Ingredient name

When touching the CANCEL button
Then Edit Ingredient page has changed to Ingredient page

Scenario: Done - Single add with price
Given on Ingredient page
When touching the Ingredient
Then Ingredient page has moved down to Add Menu page
And touching the DONE button
Then pop-up Updating... has a pop-up and Warning Variable menu can not enable delivery pop-up has a pop-up with OK button
And touching the Ok button
Then Warning pop-up has missing

When touching the Delivery button
Then the Delivery button has changed to grey color

When touching the DONE button
Then pop-up Updating has a pop-up and when updated a pop-up has missing


Scenario: Cancel - Single add with price
Given on Ingredient page
When touching the Ingredient
Then Ingredient page has moved down to Add Menu page
And touching the CANCEL button
Then Add Menu page has moved down with missing