Feature: Move to category
Scenario: Done - Move to category
Given on Edit Menu page
When touching the pencil button
Then Edit Menu button, Move to category,  and Remove from category has a pop-up
And touching the Move to category button
Then Edit Menu page has changed to Move to category page and show a menu, not in a category

When touching the Search textbox
Then can edit and the keyboard has moved up from the bottom side
And entering some word or number 
Then show the menu that is similar to the name entered
And fix it back to normal

When touching the ทort by Create Time drop-down
Then the Sort by Create Time drop-down has a drop-down to show Sort by Create Time, Sort by Name A-Z, Sort by Name Z-A, Sort by Category A-Z, Sort by Category A-Z, Sort by High Value, Sort by Low Value, and Sort by Menu ID
And select Sort by Name A-Z
Then menu changes the arrangement according to the name to A-Z

When touching the Sort by Create Time drop-down
Then the Sort by Create Time drop-down has a drop-down to show Sort by Create Time, Sort by Name A-Z, Sort by Name Z-A, Sort by Category A-Z, Sort by Category A-Z, Sort by High Value, Sort by Low Value, and Sort by Menu ID
And select Sort by Name Z-A
Then menu changes the arrangement according to the name to Z-A

When touching the Sort by Create Time drop-down
Then the Sort by Create Time drop-down has a drop-down to show Sort by Create Time, Sort by Name A-Z, Sort by Name Z-A, Sort by Category A-Z, Sort by Category A-Z, Sort by High Value, Sort by Low Value, and Sort by Menu ID
And select Sort by Category A-Z
Then menu changes the arrangement according to the category to A-Z

When touching the Sort by Create Time drop-down
Then the Sort by Create Time drop-down has a drop-down to show Sort by Create Time, Sort by Name A-Z, Sort by Name Z-A, Sort by Category A-Z, Sort by Category A-Z, Sort by High Value, Sort by Low Value, and Sort by Menu ID
And select Sort by Category Z-A
Then menu changes the arrangement according to the category to Z-A

When touching the Sort by Create Time drop-down
Then the Sort by Create Time drop-down has a drop-down to show Sort by Create Time, Sort by Name A-Z, Sort by Name Z-A, Sort by Category A-Z, Sort by Category A-Z, Sort by High Value, Sort by Low Value, and Sort by Menu ID
And select Sort by High Value
Then the menu changes the arrangement according to the High Value to the high-low

When touching the Sort by Create Time drop-down
Then the Sort by Create Time drop-down has a drop-down to show Sort by Create Time, Sort by Name A-Z, Sort by Name Z-A, Sort by Category A-Z, Sort by Category A-Z, Sort by High Value, Sort by Low Value, and Sort by Menu ID
And select Sort by Low Value
Then the menu changes the arrangement according to the Low Value to low-high

When touching the Sort by Create Time drop-down
Then the Sort by Create Time drop-down has a drop-down to show Sort by Create Time, Sort by Name A-Z, Sort by Name Z-A, Sort by Category A-Z, Sort by Category A-Z, Sort by High Value, Sort by Low Value, and Sort by Menu ID
And select Sort by Menu ID
Then menu changes the arrangement according to the Menu ID to A-Z form Menu ID
And fix it back to normal

When touching the menu
Then green checkmark has show on the front of the menu and show green color on the row of the menu

Scenario: Done and confirm
Given on Move to category page
When touching the DONE button
Then Move to category pop-up has moved up from the bottom side
And select some category
Then green checkmark has show on the back of the category you select

When touching the COMFIRM button
Then toast Updating… has show up and when updated the Move to category page has changed to the Edit Menu page with Menu you select has move to category you select


Scenario: Done - Cancel - Cancel
Given on Move to category page
When touching the DONE button
Then Move to category pop-up has moved up from the bottom side
And select some category
Then green checkmark has show on the back of the category you select

When touching the CANCEL button
Then Move to category pop-up has moved down and missing
And touching the CANCEL button 
Then the Move to category page has changed to the Edit Menu page


Scenario: Cancel
Given on Move to category page
When touching the CANCEL button
Then Move to category pop-up has moved down and missing
And touching the CANCEL button 
Then the Move to category page has changed to the Edit Menu page


Scenario: not select category
Given on Move to category page
When touching the menu
Then green checkmark has show on the front of the menu and show green color on the row of the menu
And touching the DONE button 
Then Move to category pop-up has moved up from the bottom side
And touching the COMFIRM button
Then Warning Please select category to move has a pop-up with OK button
And touching the OK button
Then Warning pop-up has missing

