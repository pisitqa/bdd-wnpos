Feature: Remove from category
Scenario: Done - Move to category
Given on Edit Menu page
When touching the pencil button
Then Edit Menu button, Move to category,  and Remove from category has a pop-up
And touching the Remove from category button
Then Edit Menu page has changed to Remove from category page and show a menu in a category

When touching the Search textbox
Then can edit and the keyboard has moved up from the bottom side
And entering some word or number 
Then show the menu that is similar to the name entered
And fix it back to normal

When touching the Sort by Create Time drop-down
Then the Sort by Create Time drop-down has a drop-down to show Sort by Create Time, Sort by Name A-Z, Sort by Name Z-A, Sort by Category A-Z, Sort by Category A-Z, Sort by High Value, Sort by Low Value, and Sort by Menu ID
And select Sort by Name A-Z
Then menu changes the arrangement according to the name to A-Z

When touching the Sort by Create Time drop-down
Then the Sort by Create Time drop-down has a drop-down to show Sort by Create Time, Sort by Name A-Z, Sort by Name Z-A, Sort by Category A-Z, Sort by Category A-Z, Sort by High Value, Sort by Low Value, and Sort by Menu ID
And select Sort by Name Z-A
Then menu changes the arrangement according to the name to Z-A

When touching the Sort by Create Time drop-down
Then the Sort by Create Time drop-down has a drop-down to show Sort by Create Time, Sort by Name A-Z, Sort by Name Z-A, Sort by Category A-Z, Sort by Category A-Z, Sort by High Value, Sort by Low Value, and Sort by Menu ID
And select Sort by Category A-Z
Then menu changes the arrangement according to the category to A-Z

When touching the Sort by Create Time drop-down
Then the Sort by Create Time drop-down has a drop-down to show Sort by Create Time, Sort by Name A-Z, Sort by Name Z-A, Sort by Category A-Z, Sort by Category A-Z, Sort by High Value, Sort by Low Value, and Sort by Menu ID
And select Sort by Category Z-A
Then menu changes the arrangement according to the category to Z-A

When touching the Sort by Create Time drop-down
Then the Sort by Create Time drop-down has a drop-down to show Sort by Create Time, Sort by Name A-Z, Sort by Name Z-A, Sort by Category A-Z, Sort by Category A-Z, Sort by High Value, Sort by Low Value, and Sort by Menu ID
And select Sort by High Value
Then the menu changes the arrangement according to the High Value to the high-low

When touching the Sort by Create Time drop-down
Then the Sort by Create Time drop-down has a drop-down to show Sort by Create Time, Sort by Name A-Z, Sort by Name Z-A, Sort by Category A-Z, Sort by Category A-Z, Sort by High Value, Sort by Low Value, and Sort by Menu ID
And select Sort by Low Value
Then the menu changes the arrangement according to the Low Value to low-high

When touching the Sort by Create Time drop-down
Then the Sort by Create Time drop-down has a drop-down to show Sort by Create Time, Sort by Name A-Z, Sort by Name Z-A, Sort by Category A-Z, Sort by Category A-Z, Sort by High Value, Sort by Low Value, and Sort by Menu ID
And select Sort by Menu ID
Then menu changes the arrangement according to the Menu ID to A-Z form Menu ID
And fix it back to normal

When touching the menu
Then orange checkmark has show on the front of the menu and show orange color on the row of the menu


Scenario: Done and confirm
Given on Remove from category page
When touching the DONE button
Then Deactive menu Are you sure to deactivate menu has a pop-up with OK button and CANCEL button
And touching the OK button
Then toast Updating… has a pop-up and when updated pop-up has missing

Scenario: Done - Cancel
Given on Remove from category page
When touching the DONE button
Then Deactive menu Are you sure to deactivate menu has a pop-up with OK button and CANCEL button
And touching the OK button

When touching the CANCEL button
Then Deactive menu pop-up has missing
And touching the CANCEL button 
Then the Remove from category page has changed to the Edit Menu page


Scenario: Cancel
Given on Remove from category page
When touching the CANCEL button
Then the Remove from category page has changed to the Edit Menu page


Scenario: not select menu
Given on Remove from category
When touching the CONFIRM button
Then Warning Please select menu has a pop-up with OK button
And touching the OK button 
Then Warning pop-up has missing


