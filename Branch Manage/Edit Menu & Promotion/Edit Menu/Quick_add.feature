Feature: Quick add
Scenario: Quick add with price
Given on Edit Menu page
When touching the paper button on the top of the right side
Then Single add button and Quick add button has a pop-up

When touching the Quick add
Then Edit Menu page has changed to Quick add with Quick add page has moved up from the bottom side and Move to Category pop-up has moved up
And select some Category on the Move to Category pop-up
Then touching the CONFIRM button
And Move to Category pop-up has moved down and name of Category you select has shown on Move for Category on the top of the left side


Scenario: allow photo form Camera with OK button
Given on Quick add
When touching the + box
Then the Camera button, Library button, and Gallery button has a pop-up
And touching the Camera button
Then Warning Please allow camera access from device setting has a pop-up with the OK button and “Wongnai POS” Would Like to Access the Camera. This app need to take photo from camera to upload menu picture has a pop-up on the Warning pop-up with OK button and Don’t Allow button
And touching an OK button
Then “Wongnai POS” pop-up has missing
And touching the OK button on the Warning pop-up
Then Warning pop-up has missing


Scenario: add a picture done form camera
Given on Add Menu
When touching the + box
Then the Camera button, Library button, and Gallery button has a pop-up
And touching the Camera button
Then the Camera box has moved up from the bottom side
And touching the circle button on the center
Then show example picture with DONE button and CANCEL button
And select the DONE button 
Then show up on the + box


Scenario: Cancel 1 - add a picture from the camera
Given on Add Menu
When touching the + box
Then the Camera button, Library button, and Gallery button has a pop-up
And touching the Camera button
Then the Camera box has moved up from the bottom side
And touching the arrow button on the left side
Then the Camera box has missing


Scenario: Cancel 2 - add a picture from the camera
Given on Add Menu
When touching the + box
Then the Camera button, Library button, and Gallery button has a pop-up
And touching the Camera button
Then the Camera box has moved up from the bottom side
And touching the circle button on the center
Then show example picture with DONE button and CANCEL button
And select the CANCEL button 
Then show up on the + box


Scenario: don’t allow photo form Camera with Don’t Allow
When touching the + box
Then the Camera button, Library button, and Gallery button has a pop-up
And touching the Camera button
Then Warning Please allow camera access from device setting has a pop-up with the OK button and “Wongnai POS” Would Like to Access the Camera. This app need to take photo from camera to upload menu picture has a pop-up on the Warning pop-up with OK button and Don’t Allow button
And touching a Don’t Allow button
Then “Wongnai POS” pop-up has missing
And touching the OK button on the Warning pop-up
Then Warning pop-up has missing
And touching the + box
Then the Camera button, Library button, and Gallery button has a pop-up
And touching the Camera button
Then Warning Please allow camera access from device setting has a pop-up with the OK button
And touching the OK button and go to setting of your device
Then Scroll down and Select Wongnai POS
And touching the Camera toggle and move to Wongnai POS app
Then PIN and go to Add Menu page
And touching the + box
Then the Camera button, Library button, and Gallery button has a pop-up
And touching the Camera button
Then the Camera box has moved up from the bottom side


Scenario: add a photo from Library done
Given on Add Menu page
When touching the + box
Then the Camera button, Library button, and Gallery button has a pop-up
And select the Library button
Then button pop-up has changed to Photo pop-up with All Photos, Recents, and album you created
And select some album
Then album changed to photo in the album you select
And select some photo
Then the photo box has moved up from the bottom side with the DONE button, and CANCEL
And select the DONE button
Then the photo box has missing and photo you select has shown up on + box


Scenario: Cancel - add a photo from Library
Given on Add Menu page
When touching the + box
Then the Camera button, Library button, and Gallery button has a pop-up
And select the Library button
Then button pop-up has changed to Photo pop-up with All Photos, Recents, and album you created
And select some album
Then album changed to photo in the album you select
And select some photo
Then the photo box has moved up from the bottom side with the DONE button, and CANCEL
And select the CANCEl button
Then the photo box has missed and no photo on the + box


Scenario: add data
Given on Add Menu
When touching the Menu name 1
Then can edit and the keyboard has faded up from the bottom side
And entering some word or number in the Menu name 1 textbox
Then some word or number has shown up on the Menu name 1 textbox 

When touching the  Menu name 2
Then can edit and the keyboard has faded up from the bottom side
And entering some word or number in the Menu name 2 textbox
Then some word or number has shown up on the Menu name 2 textbox 

When touching the price textbox
Then can edit and the keyboard has faded up from the bottom side
And entering some number in the price textbox
Then some number has shown up on the price textbox

When touching the Non-VAT checkbox 
Then the green checkmark has on the Non-VAT checkbox 

When touching the Menu ID textbox
Then can edit and the keyboard has faded up from the bottom side
And entering some word and number in the Menu ID textbox 
Then some word and number has shown up on the Menu ID textbox

When touching the Delivery button 
Then the Delivery button has changed to grey color from the green color

When touching the Recommend button
Then the Recommend button has changed to light blue color from the grey color


Scenario: Done - Quick add with price
Given on Quick add page
When touching the DONE button
Then toast Saving... has shown up and when saved toast has missing


Scenario: Cancel - Quick add with price
Given on Quick add page
When touching the CANCEL button
Then Quick add page has moved down with missing


Scenario: Done - Quick add with variable price
Given on Quick add page
When touching the paper button on the top of the right side
Then Single add button and Quick add button has a pop-up

When touching the Quick add button
Then the Add Menu page has moved up from the bottom side


Scenario: allow photo form Camera with OK button
When touching the + box
Then the Camera button, Library button, and Gallery button has a pop-up
And touching the Camera button
Then Warning Please allow camera access from device setting has a pop-up with the OK button and “Wongnai POS” Would Like to Access the Camera. This app need to take photo from camera to upload menu picture has a pop-up on the Warning pop-up with OK button and Don’t Allow button
And touching an OK button
Then “Wongnai POS” pop-up has missing
And touching the OK button on the Warning pop-up
Then Warning pop-up has missing


Scenario: add a picture done form camera
Given on Add Menu
When touching the + box
Then the Camera button, Library button, and Gallery button has a pop-up
And touching the Camera button
Then the Camera box has moved up from the bottom side
And touching the circle button on the center
Then show example picture with DONE button and CANCEL button
And select the DONE button 
Then show up on the + box


Scenario: Cancel 1 - add a picture from the camera
Given on Add Menu
When touching the + box
Then the Camera button, Library button, and Gallery button has a pop-up
And touching the Camera button
Then the Camera box has moved up from the bottom side
And touching the arrow button on the left side
Then the Camera box has missing


Scenario: Cancel 2 - add a picture from the camera
Given on Add Menu
When touching the + box
Then the Camera button, Library button, and Gallery button has a pop-up
And touching the Camera button
Then the Camera box has moved up from the bottom side
And touching the circle button on the center
Then show example picture with DONE button and CANCEL button
And select the CANCEL button 
Then show up on the + box


Scenario: don’t allow photo form Camera with Don’t Allow
When touching the + box
Then the Camera button, Library button, and Gallery button has a pop-up
And touching the Camera button
Then Warning Please allow camera access from device setting has a pop-up with the OK button and “Wongnai POS” Would Like to Access the Camera. This app need to take photo from camera to upload menu picture has a pop-up on the Warning pop-up with OK button and Don’t Allow button
And touching a Don’t Allow button
Then “Wongnai POS” pop-up has missing
And touching the OK button on the Warning pop-up
Then Warning pop-up has missing
And touching the + box
Then the Camera button, Library button, and Gallery button has a pop-up
And touching the Camera button
Then Warning Please allow camera access from device setting has a pop-up with the OK button
And touching the OK button and go to setting of your device
Then Scroll down and Select Wongnai POS
And touching the Camera toggle and move to Wongnai POS app
Then PIN and go to Add Menu page
And touching the + box
Then the Camera button, Library button, and Gallery button has a pop-up
And touching the Camera button
Then the Camera box has moved up from the bottom side


Scenario: add a photo from Library done
Given on Add Menu page
When touching the + box
Then the Camera button, Library button, and Gallery button has a pop-up
And select the Library button
Then button pop-up has changed to Photo pop-up with All Photos, Recents, and album you created
And select some album
Then album changed to photo in the album you select
And select some photo
Then the photo box has moved up from the bottom side with the DONE button, and CANCEL
And select the DONE button
Then the photo box has missing and photo you select has shown up on + box


Scenario: Cancel - add a photo from Library
Given on Add Menu page
When touching the + box
Then the Camera button, Library button, and Gallery button has a pop-up
And select the Library button
Then button pop-up has changed to Photo pop-up with All Photos, Recents, and album you created
And select some album
Then album changed to photo in the album you select
And select some photo
Then the photo box has moved up from the bottom side with the DONE button, and CANCEL
And select the CANCEl button
Then the photo box has missed and no photo on the + box


Scenario: add data
Given on Add Menu
When touching the Menu name 1
Then can edit and the keyboard has faded up from the bottom side
And entering some word or number in the Menu name 1 textbox
Then some word or number has shown up on the Menu name 1 textbox 

When touching the  Menu name 2
Then can edit and the keyboard has faded up from the bottom side
And entering some word or number in the Menu name 2 textbox
Then some word or number has shown up on the Menu name 2 textbox 

When touching the Variable checkbox
Then green checkmark has shown on the Variable checkbox

When touching the Non-VAT checkbox 
Then the green checkmark has on the Non-VAT checkbox 

When touching the Menu ID textbox
Then can edit and the keyboard has faded up from the bottom side
And entering some word and number in the Menu ID textbox 
Then some word and number has shown up on the Menu ID textbox

When touching the Delivery button 
Then the Delivery button has changed to grey color from the green color

When touching the Recommend button
Then the Recommend button has changed to light blue color from the grey color

When touching the DONE button
Then Save menu Are you sure to save all menu? has a pop-up with OK button and CANCEL button and Warning Variable menu can not enable delivery has a pop-up with OK button
And touching the OK button
Then Warning pop-up has missing
And touching the Delivery button
Then the Delivery button has changed to grey color

When touching the DONE button
Then toast Saving... has shown up and when saved toast has missing


Scenario: Cancel - Quick add with price
Given on Quick add page
When touching the CANCEL button
Then Quick add page has moved down with missing

