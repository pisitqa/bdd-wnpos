Scenario: All bills page
Given on storefront sidebar
When touching the Bill on storefront sidebar 
Then the Bill page has to fade up from the bottom side


Scenario: coin button
When touching the coin button
Then Total Sale has a pop-up and shows Drawer ID, DD/MM/YY hh:mm:ss, Total Sales, Close bill, and Outstanding
And check a word on this page
Then touching the CLOSE button
And pop-up has close


Scenario: entering a correct PIN on Bill page
When touching the Drawer button
Then PIN page has shown up
And entering the correct PIN
Then a drawer you connect with a printer has a kick-off


Scenario: Open bill button
Given on All bills page
When touching the Closed button
Then Open bill button and sidebar has changed to Closed bill button with orange color and sidebar
And touching the Open button
Then Closed bill button and sidebar has changed to Open bill button with orange color and sidebar

When touching the Search textbox
Then can entering some word
And entering some word
Then show up the word you entering with the x button on the back textbox
And touching the x button
Then some word has missing

When touching the table button
Then the table button has changed to orange color
And touching the table button again
Then the table button has changed to white color

When touching the bag button
Then the bag button has changed to orange color
And touching the bag button again
Then the bag button has changed to white color

When touching the motorcycle button
Then the motorcycle button has changed to orange color
And touching the motorcycle button again
Then the motorcycle button has changed to white color

When touching the Closed button
Then Open bill button and sidebar has changed to Closed bill button with orange color and sidebar
And touching the drop-down
Then show DD MM YYYY with present and old

When selecting someday some month and some year
Then toast Loading… has shown up and missing when load passed
